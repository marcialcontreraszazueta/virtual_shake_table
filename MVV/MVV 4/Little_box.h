//
//  Header.h
//  Cont3
//
//  Created by Marcial Alberto Contreras Zazueta on 5/24/11.
//  Copyright 2011 UNAM Facultad de Ingeniería. All rights reserved.
//

//#include "btBulletDynamicsCommon.h"
#include <BulletDynamics/btBulletDynamicsCommon.h>


btRigidBody *importar_Little_box(btScalar conctact_threshold){
    // ................ CAJA .................
    btCollisionShape* librero =  new btBoxShape(btVector3(btScalar(0.2),btScalar(0.2),btScalar(1.5)));
    
    //librero->setMargin(1.f);
    //m_collisionShapes.push_back(librero); 
    btTransform libreroTransform;
    libreroTransform.setIdentity();
    
        btScalar mass(1.f);
        bool isDynamic = (mass != 0.f);
        
        btVector3 localInertia(0,0,0);
        if (isDynamic)
            librero->calculateLocalInertia(mass,localInertia);
        libreroTransform.setOrigin(btVector3(btScalar(1.),btScalar(2.),btScalar(1.5)));
        btDefaultMotionState* myMotionState1 = new btDefaultMotionState(libreroTransform);
        btRigidBody::btRigidBodyConstructionInfo rbInfo(mass,myMotionState1,librero,localInertia);
        rbInfo.m_restitution = btScalar(0.3);
        rbInfo.m_linearDamping = btScalar(0.05);
        rbInfo.m_friction = btScalar(0.8);
        btRigidBody* testBody = new btRigidBody(rbInfo);
        //testBody->setContactProcessingThreshold(m_defaultContactProcessingThreshold);
        testBody->setContactProcessingThreshold(conctact_threshold);
        //m_dynamicsWorld->addRigidBody(testBody);
    
    return testBody;
    
}