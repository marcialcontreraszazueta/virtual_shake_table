//
//  Sismo.h
//  Cont3
//
//  Created by Marcial Alberto Contreras Zazueta on 5/12/11.
//  Copyright 2011 UNAM Facultad de Ingeniería. All rights reserved.
//
#include <vector>

#ifndef SISMO_H
#define SISMO_H

#define sismo_en_desplazamiento

class terremoto {

public:

    long N_dat;
    float DT;
    
    std::vector<float> Xs;
    std::vector<float> Ys;
    std::vector<float> Zs;

};

/*
struct eq {
    float *S;
    //float *desp;
    //float *rawSignal;
    long N_dat;
    float DT;
};
*/
long cant_datos(const char * ruta, bool binario);

std::vector<float> sismo(const char * ruta, float DT,long tamano, bool binario, bool Integra, float escala);


float TrapeciosRule(float aa, float b, float N, float *S);
float Trapecio(float aa, float b, float N, std::vector<float> S);

#endif //SISMO_H
