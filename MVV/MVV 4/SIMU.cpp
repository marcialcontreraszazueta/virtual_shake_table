/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2003-2006 Erwin Coumans  http://continuousphysics.com/Bullet/

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose, 
including commercial applications, and to alter it and redistribute it freely, 
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/
#include "blender.h"
#include "bMain.h"
#include "bBlenderFile.h"

#ifdef __APPLE__
#include <strings.h>
#define stricmp strcasecmp
#endif
///create 125 (5x5x5) dynamic object
#define ARRAY_SIZE_X 45
#define ARRAY_SIZE_Y 45
#define ARRAY_SIZE_Z 45
#define VOXEL_SIZE_START .05
//maximum number of objects (and allow user to shoot additional boxes)
#define MAX_PROXIES (ARRAY_SIZE_X*ARRAY_SIZE_Y*ARRAY_SIZE_Z + 1024)

//#define SWAP_COORDINATE_SYSTEMS
#ifdef SWAP_COORDINATE_SYSTEMS

#define IRR_X 0
#define IRR_Y 2
#define IRR_Z 1

#define IRR_X_M 1.f
#define IRR_Y_M 1.f
#define IRR_Z_M 1.f

///also winding is different
#define IRR_TRI_0_X 0
#define IRR_TRI_0_Y 2
#define IRR_TRI_0_Z 1

#define IRR_TRI_1_X 0
#define IRR_TRI_1_Y 3
#define IRR_TRI_1_Z 2
#else
#define IRR_X 0
#define IRR_Y 1
#define IRR_Z 2

#define IRR_X_M 1.f
#define IRR_Y_M 1.f
#define IRR_Z_M 1.f

///also winding is different
#define IRR_TRI_0_X 0
#define IRR_TRI_0_Y 1
#define IRR_TRI_0_Z 2

#define IRR_TRI_1_X 0
#define IRR_TRI_1_Y 2
#define IRR_TRI_1_Z 3
#endif


///scaling of the objects (0.1 = 20 centimeter boxes )
#define SCALING 1.
#define START_POS_X -5
#define START_POS_Y -5
#define START_POS_Z -3
#include <stdio.h>

#ifdef USE_GRAPHICS_OBJECTS

#define NOR_SHORTTOFLOAT 32767.0f
void norShortToFloat(const short *shnor, float *fnor)
{
	fnor[0] = shnor[0] / NOR_SHORTTOFLOAT;
	fnor[1] = shnor[1] / NOR_SHORTTOFLOAT;
	fnor[2] = shnor[2] / NOR_SHORTTOFLOAT;
}


#define XMD_H
#ifdef  __cplusplus
extern "C" {
#endif


#include "jpeglib/jpeglib.h" // use included jpeglib

#ifdef  __cplusplus
}
#endif


#include <setjmp.h>
#endif //USE_GRAPHICS_OBJECTS

#include "GLDebugFont.h"

#include "BulletCollision/CollisionDispatch/SphereTriangleDetector.h"

//#include "BunnyMesh.h"
#include "SIMU.h"
#include "GlutStuff.h"
///btBulletDynamicsCommon.h is the main Bullet include file, contains most common include files.
#include <stdio.h> //printf debugging
#include "BulletCollision/CollisionShapes/btTriangleShape.h"

///includes for reading .blend files
#include "BulletBlendReaderNew.h"
#define FORCE_ZAXIS_UP 1
#include "LinearMath/btHashMap.h"
#include "readblend.h"
#include "blendtype.h"

#include "KinemMot.h"
#include <sys/time.h>
#include <iostream>

#include "Little_box.h"

#include "LinearMath/btSerializer.h"
#include "time.h"

#define borra_piso
#define ver_posicion
#define solo_sismo_en_x

static int sho = 1; 

void SIMU::somethingMore() {
    //aqui van cosas extra que hacer después de initphysics
    //es pa' probar código experimental y radioactivo
    
    /*
    //los cuadros
    int i;
    for (i=0;i<cant_de_objetos;i++){
        
        std::cout<<"\nIN";
       
        if (strncmp(Objetos[i].nombre, "OBcuadro", 8)==0) {
            //decidimos las posición del pivote
            btCollisionObject* colObj = m_dynamicsWorld->getCollisionObjectArray()[i];
            btTransform local = colObj->getWorldTransform();
            btCollisionShape* colsh = colObj->getCollisionShape();
            
            btVector3 aabbMin;
            btVector3 aabbMax;
            
            colsh->getAabb(local, aabbMin, aabbMax);
            std::cout<<"\nCuadros colgantes\n";
            std::cout<<"\n "<<Objetos[i].nombre<<"aabb:";
            std::cout<<"\nMin "<<aabbMin[0]<<"\t"<<aabbMin[1]<<"\t"<<aabbMin[2]<<"\t";
            std::cout<<"\nMax "<<aabbMax[0]<<"\t"<<aabbMax[1]<<"\t"<<aabbMax[2]<<"\t";
            float x = (aabbMax[0]-aabbMin[0])*0.5+aabbMin[0];
            float y = (aabbMax[1]-aabbMin[1])*0.5+aabbMin[1];
            float z = (aabbMax[2]-aabbMin[2])*0.9+aabbMin[2];
            
            std::cout<<"\nPivote ("<<x<<","<<y<<","<<z<<")";
            const btVector3 Pivote(x,y,z);
            
            btRigidBody* rb = localCreateRigidBody(1.f, local, colsh);
            
            btTypedConstraint* clavo = new btPoint2PointConstraint(*rb, Pivote);
            m_dynamicsWorld->addConstraint(clavo,true);
            clavo->setDbgDrawSize(5.f);
            
           
        
        }
        }
        */
      /*  
        { 
            // create a Hinge2 joint
            // create two rigid bodies
            // static bodyA (parent) on top:
            btTransform tr;
            tr.setIdentity();
            btCollisionShape* shape = new btBoxShape(btVector3(.1f,.1f,.1f));
            m_collisionShapes.push_back(shape);
            tr.setOrigin(btVector3(btScalar(0.), btScalar(4.), btScalar(3.)));
            btRigidBody* pBodyA = localCreateRigidBody( 0.0, tr, shape);
            pBodyA->setActivationState(DISABLE_DEACTIVATION);
            // dynamic bodyB (child) below it :
            tr.setIdentity();
            tr.setOrigin(btVector3(btScalar(0.), btScalar(0.), btScalar(3.)));
            btRigidBody* pBodyB = localCreateRigidBody(1.0, tr, shape);
            pBodyB->setActivationState(DISABLE_DEACTIVATION);
            // add some data to build constraint frames
            btVector3 parentAxis(0.f, 1.f, 0.f); 
            btVector3 childAxis(1.f, 0.f, 0.f); 
            btVector3 anchor(0.f, 0.f, 3.f);
            btHinge2Constraint* pHinge2 = new btHinge2Constraint(*pBodyA, *pBodyB, anchor, parentAxis, childAxis);
            pHinge2->setLowerLimit(-SIMD_HALF_PI * 0.5f);
            pHinge2->setUpperLimit( SIMD_HALF_PI * 0.5f);
            // add constraint to world
            m_dynamicsWorld->addConstraint(pHinge2, true);
            // draw constraint frames and limits for debugging
            pHinge2->setDbgDrawSize(btScalar(5.f));
        }
        
    */
}


void SIMU::keyboardCallback(unsigned char key, int x, int y) {
    //printf("\nkey %hhu",key);
    float step = 0.2;
    
    if (key == 'd') {
        //derecha
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        m_cameraTargetPosition = cameraPos;
        //std::cout<<"\n"<<cameraPos[0]<<"   "<<cameraPos[1]<<"   "<<cameraPos[2];
        //std::cout<<"\n"<<m_cameraTargetPosition[0]<<"   "<<m_cameraTargetPosition[1]<<"   "<<m_cameraTargetPosition[2];
        m_cameraPosition[0] = m_cameraTargetPosition[0] + step;
        m_cameraPosition[1] = m_cameraTargetPosition[1] + 0.;
        m_cameraPosition[2] = m_cameraTargetPosition[2] + 0.;
        cameraPos=m_cameraPosition;
        //std::cout<<"\n\n"<<cameraPos[0]<<"   "<<cameraPos[1]<<"   "<<cameraPos[2];
        //std::cout<<"\n"<<m_cameraTargetPosition[0]<<"   "<<m_cameraTargetPosition[1]<<"   "<<m_cameraTargetPosition[2];
        
        gluLookAt(m_cameraPosition[0], m_cameraPosition[1], m_cameraPosition[2],
                  m_cameraTargetPosition[0], m_cameraTargetPosition[1], m_cameraTargetPosition[2],
                  m_cameraUp.getX(), m_cameraUp.getY(), m_cameraUp.getZ());
        
    } else if (key == 'a') {
        //izq
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        
        m_cameraTargetPosition = cameraPos;
        
        m_cameraPosition[0] = m_cameraTargetPosition[0] - step;
        m_cameraPosition[1] = m_cameraTargetPosition[1] + 0.;
        m_cameraPosition[2] = m_cameraTargetPosition[2] + 0.;
        cameraPos=m_cameraPosition;
        
        gluLookAt(m_cameraPosition[0], m_cameraPosition[1], m_cameraPosition[2],
                  m_cameraTargetPosition[0], m_cameraTargetPosition[1], m_cameraTargetPosition[2],
                  m_cameraUp.getX(), m_cameraUp.getY(), m_cameraUp.getZ());
    }else if (key == 'w') {
        //adelante
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        
        m_cameraTargetPosition = cameraPos;
        
        m_cameraPosition[0] = m_cameraTargetPosition[0] + 0.;
        m_cameraPosition[1] = m_cameraTargetPosition[1] + step;
        m_cameraPosition[2] = m_cameraTargetPosition[2] ;
        cameraPos=m_cameraPosition;
        
        gluLookAt(m_cameraPosition[0], m_cameraPosition[1], m_cameraPosition[2],
                  m_cameraTargetPosition[0], m_cameraTargetPosition[1], m_cameraTargetPosition[2],
                  m_cameraUp.getX(), m_cameraUp.getY(), m_cameraUp.getZ());
    }else if (key == 's') {
        //atras
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        
        m_cameraTargetPosition = cameraPos;
        
        m_cameraPosition[0] = m_cameraTargetPosition[0] + 0.;
        m_cameraPosition[1] = m_cameraTargetPosition[1] - step;
        m_cameraPosition[2] = m_cameraTargetPosition[2] ;
        cameraPos=m_cameraPosition;
        
        gluLookAt(m_cameraPosition[0], m_cameraPosition[1], m_cameraPosition[2],
                  m_cameraTargetPosition[0], m_cameraTargetPosition[1], m_cameraTargetPosition[2],
                  m_cameraUp.getX(), m_cameraUp.getY(), m_cameraUp.getZ());
    }else if (key == ' '){
        //reset
        clientResetScene();
        //exitPhysics();
        initPhysics();
    }else{
        if (key == 'r' ) {
            key = 'a';
        }
        if (key == 'f') {
            key = 'w';
        }
        DemoApplication::keyboardCallback(key,x,y);
    }
    
}

static bool CustomMaterialCombinerCallback(btManifoldPoint& cp,	const btCollisionObject* colObj0,int partId0,int index0,const btCollisionObject* colObj1,int partId1,int index1) {
	cp.m_lateralFrictionInitialized = true;
	cp.m_lateralFrictionDir1.setValue(1,0,0);
	///downscale the friction direction 2 for lower (anisotropic) friction (rather than a unit vector)
	cp.m_lateralFrictionDir2.setValue(0,0,0.1);
	///choose a target velocity in the friction dir1 direction, for a conveyor belt effect
	//cp.m_contactMotion1 = 1.f;
	//cp.m_contactCFM2 = 0.1;
	//cp.m_combinedFriction = 10;
	//cp.m_combinedRestitution = calculateCombinedRestitution(restitution0,restitution1);
	return true;
}

void TickCallback(btDynamicsWorld *world, btScalar timeStep){
    if (sho == 1) {
        timeval current_time;
        gettimeofday(&current_time, NULL);
        
        long sec = current_time.tv_sec;
        long usec = current_time.tv_usec;
        
        printf("\n%0.3f internal sim.step at systime:%f",timeStep,(sec*1000 + usec/1000.0)+0.5);
        
        time_t tim=time(NULL);
        tm *now=localtime(&tim);
        printf("\t %02d:%02d:%02d",now->tm_hour,now->tm_min,now->tm_sec);
    }
}

void SIMU::displayCallback(void) {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
	
	renderme();

	//optional but useful: debug drawing to detect problems
	if (m_dynamicsWorld)
		m_dynamicsWorld->debugDrawWorld();
    
	glFlush();
	glutSwapBuffers();
}

struct  btVoxelizationCallback : public btTriangleCallback {
	//btCompoundShape* m_bunnyCompound;
	btSphereShape*	m_sphereChildShape;
	btVector3	m_curSpherePos;
	bool	m_oncePerSphere;

	virtual void processTriangle(btVector3* triangleVerts, int partId, int triangleIndex)
	{
		if (!m_oncePerSphere)
		{
			btTransform tr;
			tr.setIdentity();
			tr.setOrigin(m_curSpherePos);

			btTriangleShape triangleShape(triangleVerts[0],triangleVerts[1],triangleVerts[2]);
			SphereTriangleDetector detector(m_sphereChildShape,&triangleShape,0.);
			btVector3 hitPos,hitNormal;
			btScalar hitDepth,timeOfImpact;
			if (detector.collide(m_curSpherePos,hitPos,hitNormal,hitDepth,timeOfImpact,0.))
			{
				m_oncePerSphere = true;
				//m_bunnyCompound->addChildShape(tr,m_sphereChildShape);
			}
		}
	}
};

#ifdef USE_GRAPHICS_OBJECTS
// struct for handling jpeg errors
struct my_jpeg_error_mgr {
    // public jpeg error fields
    struct jpeg_error_mgr pub;

    // for longjmp, to return to caller on a fatal error
    jmp_buf setjmp_buffer;

};

void error_exit (j_common_ptr cinfo) {
	// unfortunately we need to use a goto rather than throwing an exception
	// as gcc crashes under linux crashes when using throw from within
	// extern c code

	// Always display the message
	(*cinfo->err->output_message) (cinfo);

	// cinfo->err really points to a irr_error_mgr struct
//	irr_jpeg_error_mgr *myerr = (irr_jpeg_error_mgr*) cinfo->err;
//	longjmp(myerr->setjmp_buffer, 1);
	exit(0);
}

void output_message(j_common_ptr cinfo) {
	// display the error message.
//	unsigned char temp1[JMSG_LENGTH_MAX];
//	(*cinfo->err->format_message)(cinfo, temp1);
	printf("JPEG FATAL ERROR");//,temp1, ELL_ERROR);
}

void init_source (j_decompress_ptr cinfo) {
	// DO NOTHING
}

void skip_input_data (j_decompress_ptr cinfo, long count) {
	jpeg_source_mgr * src = cinfo->src;
	if(count > 0)
	{
		src->bytes_in_buffer -= count;
		src->next_input_byte += count;
	}
}

void term_source (j_decompress_ptr cinfo) {
	// DO NOTHING
}

boolean fill_input_buffer (j_decompress_ptr cinfo) {
	// DO NOTHING
	return 1;
}

struct BasicTexture {
	unsigned char*	m_jpgData;
	int		m_jpgSize;

	int				m_width;
	int				m_height;
	GLuint			m_textureName;
	bool			m_initialized;

	//contains the uncompressed R8G8B8 pixel data
	unsigned char*	m_output;
	
	BasicTexture(unsigned char* texels,int width,int height)
		:m_jpgData(0),
		m_jpgSize(0),
		m_width(width),
		m_height(height),
		m_textureName(-1),
    m_initialized(false),
    m_output(texels)
	{

	}

	BasicTexture(unsigned char* jpgData,int jpgSize)
		: m_jpgData(jpgData),
    m_jpgSize(jpgSize),
    m_textureName(-1),
    m_initialized(false),
    m_output(0)
	{

	}

	virtual ~BasicTexture()
	{
		delete[] m_output;
	}

	//returns true if szFilename has the szExt extension
	bool checkExt(char const * szFilename, char const * szExt)
	{
		if (strlen(szFilename) > strlen(szExt))
			 {
				  char const * szExtension = &szFilename[strlen(szFilename) - strlen(szExt)];
				  if (!stricmp(szExtension, szExt))
					  return true;
			 }
		return false;
	}

	void	loadTextureMemory(const char* fileName)
	{
		if (checkExt(fileName,".JPG"))
		{
			loadJpgMemory();

		}
	}

	void	initOpenGLTexture()
	{
		if (m_initialized)
		{
			glBindTexture(GL_TEXTURE_2D,m_textureName);
		} else
		{
			m_initialized = true;
			

			glGenTextures(1, &m_textureName);
			glBindTexture(GL_TEXTURE_2D,m_textureName);
			glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
			glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
			glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR_MIPMAP_LINEAR);
			glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
			glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
			gluBuild2DMipmaps(GL_TEXTURE_2D,3,m_width,m_height,GL_RGB,GL_UNSIGNED_BYTE,m_output);
		}
			
	}

	void	loadJpgMemory()
	{
		unsigned char **rowPtr=0;
		
		

		// allocate and initialize JPEG decompression object
		struct jpeg_decompress_struct cinfo;
		struct my_jpeg_error_mgr jerr;

		//We have to set up the error handler first, in case the initialization
		//step fails.  (Unlikely, but it could happen if you are out of memory.)
		//This routine fills in the contents of struct jerr, and returns jerr's
		//address which we place into the link field in cinfo.

		cinfo.err = jpeg_std_error(&jerr.pub);
		cinfo.err->error_exit = error_exit;
		cinfo.err->output_message = output_message;

		// compatibility fudge:
		// we need to use setjmp/longjmp for error handling as gcc-linux
		// crashes when throwing within external c code
		if (setjmp(jerr.setjmp_buffer))
		{
			// If we get here, the JPEG code has signaled an error.
			// We need to clean up the JPEG object and return.

			jpeg_destroy_decompress(&cinfo);

			
			// if the row pointer was created, we delete it.
			if (rowPtr)
				delete [] rowPtr;

			// return null pointer
			return ;
		}

		// Now we can initialize the JPEG decompression object.
		jpeg_create_decompress(&cinfo);

		// specify data source
		jpeg_source_mgr jsrc;

		// Set up data pointer
		jsrc.bytes_in_buffer = m_jpgSize;
		jsrc.next_input_byte = (JOCTET*)m_jpgData;
		cinfo.src = &jsrc;

		jsrc.init_source = init_source;
		jsrc.fill_input_buffer = fill_input_buffer;
		jsrc.skip_input_data = skip_input_data;
		jsrc.resync_to_restart = jpeg_resync_to_restart;
		jsrc.term_source = term_source;

		// Decodes JPG input from whatever source
		// Does everything AFTER jpeg_create_decompress
		// and BEFORE jpeg_destroy_decompress
		// Caller is responsible for arranging these + setting up cinfo

		// read file parameters with jpeg_read_header()
		jpeg_read_header(&cinfo, TRUE);

		cinfo.out_color_space=JCS_RGB;
		cinfo.out_color_components=3;
		cinfo.do_fancy_upsampling=FALSE;

		// Start decompressor
		jpeg_start_decompress(&cinfo);

		// Get image data
		unsigned short rowspan = cinfo.image_width * cinfo.out_color_components;
		m_width = cinfo.image_width;
		m_height = cinfo.image_height;

		// Allocate memory for buffer
		m_output = new unsigned char[rowspan * m_height];

		// Here we use the library's state variable cinfo.output_scanline as the
		// loop counter, so that we don't have to keep track ourselves.
		// Create array of row pointers for lib
		rowPtr = new unsigned char* [m_height];

		for(  int i = 0; i < m_height; i++ ) //unisgned int i = 0;
			rowPtr[i] = &m_output[ i * rowspan ];

		unsigned int rowsRead = 0;

		while( cinfo.output_scanline < cinfo.output_height )
			rowsRead += jpeg_read_scanlines( &cinfo, &rowPtr[rowsRead], cinfo.output_height - rowsRead );

		delete [] rowPtr;
		// Finish decompression

		jpeg_finish_decompress(&cinfo);

		// Release JPEG decompression object
		// This is an important step since it will release a good deal of memory.
		jpeg_destroy_decompress(&cinfo);

//		// convert image
//		IImage* image = new CImage(ECF_R8G8B8,
//			core::dimension2d<s32>(width, height), output);
//
//
//		return image;

		
	}

};

struct btTexVert {
	
	btVector3		m_localxyz;
	float			m_uv1[2];
	float			m_uv2[2];
	unsigned int	m_rgba;
	float			m_tangent[4];
	btVector3		m_normal;
	short			m_flag;
	short			m_softBodyIndex;
	unsigned int	m_unit;
	unsigned int	m_origindex;
};

struct btRenderMesh {
	BasicTexture* m_texture;
	btAlignedObjectArray<btTexVert> m_vertices;
	btAlignedObjectArray<int>	m_indices;
	btCollisionObject* m_bulletObject;
	btVector3	m_scaling;

	btRenderMesh()
		:	m_texture(0),
		m_bulletObject(0)
	{
		m_scaling.setValue(1,1,1);
	}

	void	render()
	{
		
		btScalar childMat[16];
		m_bulletObject->getWorldTransform().getOpenGLMatrix(childMat);
		btMatrix3x3 mat = m_bulletObject->getWorldTransform().getBasis();
		btTransform btr = m_bulletObject->getWorldTransform();


		float m[16];
		m[0] = mat[IRR_X][IRR_X];
		m[1] = IRR_X_M*IRR_Y_M*mat[IRR_Y][IRR_X];
		m[2] = IRR_X_M*IRR_Z_M*mat[IRR_Z][IRR_X];
		m[3] = 0;

		m[4] = IRR_X_M*IRR_Y_M*mat[IRR_X][IRR_Y];
		m[5] = mat[IRR_Y][IRR_Y];
		m[6] = IRR_Y_M*IRR_Z_M*mat[IRR_Z][IRR_Y];
		m[7] = 0;

		m[8] = IRR_X_M*IRR_Z_M*mat[IRR_X][IRR_Z];
		m[9] = IRR_Y_M*IRR_Z_M*mat[IRR_Y][IRR_Z];
		m[10] = mat[IRR_Z][IRR_Z];
		m[11] = 0;
		
		m[12] = IRR_X_M*btr.getOrigin()[IRR_X];
		m[13] = IRR_Y_M*btr.getOrigin()[IRR_Y];
		m[14] = IRR_Z_M*btr.getOrigin()[IRR_Z];
		m[15] = 1;


		if (m_texture)
		{
			m_texture->initOpenGLTexture();

			glBindTexture(GL_TEXTURE_2D,m_texture->m_textureName);

			glEnable(GL_TEXTURE_2D);
			glDisable(GL_TEXTURE_GEN_S);
			glDisable(GL_TEXTURE_GEN_T);
			glDisable(GL_TEXTURE_GEN_R);
			
			glBlendFunc(GL_SRC_ALPHA,GL_ONE);
			glDepthFunc (GL_LEQUAL);
			glDisable(GL_BLEND);
			glEnable (GL_DEPTH_TEST);

			glMatrixMode(GL_TEXTURE);
			
			
			glMatrixMode(GL_MODELVIEW);


		} else
		{
			glDisable(GL_TEXTURE_2D);
		}

		glDisable(GL_LIGHTING);
		glPushMatrix();
		
		
		btglMultMatrix(m);//childMat);
		glScalef(m_scaling[0],m_scaling[1],m_scaling[2]);
		
		//glColorMask(GL_FALSE,GL_FALSE,GL_FALSE,GL_FALSE);
		
		glBegin(GL_TRIANGLES);
		
		glColor4f(1, 1, 1,1);
		
		for (int i=0;i<m_indices.size();i++)
		{
			glNormal3f(1.f,0.f,0.f);
			glTexCoord2f(m_vertices[m_indices[i]].m_uv1[0],m_vertices[m_indices[i]].m_uv1[1]);
			glVertex3f(m_vertices[m_indices[i]].m_localxyz.getX(),m_vertices[m_indices[i]].m_localxyz.getY(),m_vertices[m_indices[i]].m_localxyz.getZ());
			
		}
		glEnd();

		glPopMatrix();
		
	}
};
#endif //USE_GRAPHICS_OBJECTS

struct BasicBlendReader : public BulletBlendReaderNew {
	SIMU*	m_basicDemo;
	btTransform	m_cameraTrans;

#ifdef USE_GRAPHICS_OBJECTS
	btHashMap<btHashString,BasicTexture*> m_textures;

	btAlignedObjectArray<btRenderMesh*> m_renderMeshes;

	BasicTexture*	m_notFoundTexture;
#endif //USE_GRAPHICS_OBJECTS


	BasicBlendReader(btDynamicsWorld* dynamicsWorld, SIMU* basicDemo)
		:BulletBlendReaderNew(dynamicsWorld),
		m_basicDemo(basicDemo),
		m_notFoundTexture(0)
	{
		m_cameraTrans.setIdentity();
	}

	virtual ~BasicBlendReader()
	{

	}
#ifdef USE_GRAPHICS_OBJECTS

	BasicTexture* findTexture(const char* fileName)
	{

		BasicTexture** result = m_textures.find(fileName);
		if (result)
			return *result;

		return 0;
	}
#endif //USE_GRAPHICS_OBJECTS


	virtual	void	convertLogicBricks() {}

	virtual	void	createParentChildHierarchy(){}

	virtual	void	addCamera(Blender::Object* tmpObject){}

	virtual	void	addLight(Blender::Object* tmpObject){}

	virtual void*	createGraphicsObject(Blender::Object* tmpObject, class btCollisionObject* bulletObject)
	{
#ifdef USE_GRAPHICS_OBJECTS

		btRenderMesh* mesh = new btRenderMesh();
		mesh->m_bulletObject = bulletObject;

		Blender::Mesh *me = (Blender::Mesh*)tmpObject->data;
		Blender::Image* image = 0;
		//bParse::bMain* mainPtr = m_blendFile->getMain();

		if (me && me->totvert  && me->totface)
		{
			if (me->totvert> 16300)
			{
				printf("me->totvert = %d\n",me->totvert);
			}

			
			int maxVerts = btMin(16300,btMax(me->totface*3*2,(me->totvert-6)));

			btTexVert* orgVertices= new btTexVert[me->totvert];
			btTexVert* newVertices= new btTexVert[maxVerts];
			

			if (!me->mvert)
				return 0;

			float nor[3] = {0.f, 0.f, 0.f};
			
			for (int v=0;v<me->totvert;v++)
			{
				float* vt3 = &me->mvert[v].co.x;
				norShortToFloat(me->mvert[v].no, nor);
				//orgVertices[v] = btTexVert(	IRR_X_M*vt3[IRR_X],	IRR_Y_M*vt3[IRR_Y],	IRR_Z_M*vt3[IRR_Z], 	nor[IRR_X], nor[IRR_Y], nor[IRR_Z], 	irr::video::SColor(255,255,255,255), 0, 1);
				orgVertices[v].m_localxyz.setValue(IRR_X_M*vt3[IRR_X],	IRR_Y_M*vt3[IRR_Y],	IRR_Z_M*vt3[IRR_Z]);
				orgVertices[v].m_normal[0] = nor[IRR_X];
				orgVertices[v].m_normal[1] = nor[IRR_Y];
				orgVertices[v].m_normal[2] = nor[IRR_Z];
			}

			
			int numVertices = 0;
			int numTriangles=0;
			int numIndices = 0;
			int currentIndex = 0;

			

			int maxNumIndices = me->totface*4*2;

			unsigned int* indices= new unsigned int[maxNumIndices];

			for (int t=0;t<me->totface;t++)
			{
				if (currentIndex>maxNumIndices)
					break;

				int v[4] = {me->mface[t].v1,me->mface[t].v2,me->mface[t].v3,me->mface[t].v4};
				
				bool smooth = me->mface[t].flag & 1; // ME_SMOOTH
				
				// if mface !ME_SMOOTH use face normal in place of vert norms
				if(!smooth)
				{
					btVector3 normal = (orgVertices[v[1]].m_localxyz-orgVertices[v[0]].m_localxyz).cross(orgVertices[v[2]].m_localxyz-orgVertices[v[0]].m_localxyz);
					normal.normalize();
					//normal.invert();
					
					orgVertices[v[0]].m_normal = normal;
					orgVertices[v[1]].m_normal = normal;
					orgVertices[v[2]].m_normal = normal;
					if(v[3])
						orgVertices[v[3]].m_normal = normal;
				}
			
				int originalIndex = v[IRR_TRI_0_X];
				indices[numIndices] = currentIndex;
				newVertices[indices[numIndices]] = orgVertices[originalIndex];
				if (me->mtface)
				{
					newVertices[indices[numIndices]].m_uv1[0] = me->mtface[t].uv[IRR_TRI_0_X][0];
					newVertices[indices[numIndices]].m_uv1[1] = 1.f - me->mtface[t].uv[IRR_TRI_0_X][1];
				} else
				{
					newVertices[indices[numIndices]].m_uv1[0] = 0;
					newVertices[indices[numIndices]].m_uv1[1] = 0;
				}
				numIndices++;
				currentIndex++;
				numVertices++;

				originalIndex = v[IRR_TRI_0_Y];
				indices[numIndices] = currentIndex;
				newVertices[indices[numIndices]] = orgVertices[originalIndex];
				if (me->mtface)
				{
					newVertices[indices[numIndices]].m_uv1[0] = me->mtface[t].uv[IRR_TRI_0_Y][0];
					newVertices[indices[numIndices]].m_uv1[1] = 1.f - me->mtface[t].uv[IRR_TRI_0_Y][1];
				} else
				{
					newVertices[indices[numIndices]].m_uv1[0] = 0;
					newVertices[indices[numIndices]].m_uv1[1] = 0;
				}
				numIndices++;
				currentIndex++;
				numVertices++;

				originalIndex = v[IRR_TRI_0_Z];
				indices[numIndices] = currentIndex;
				newVertices[indices[numIndices]] = orgVertices[originalIndex];
				if (me->mtface)
				{
					newVertices[indices[numIndices]].m_uv1[0] = me->mtface[t].uv[IRR_TRI_0_Z][0];
					newVertices[indices[numIndices]].m_uv1[1] = 1.f - me->mtface[t].uv[IRR_TRI_0_Z][1];
				} else
				{
					newVertices[indices[numIndices]].m_uv1[0] = 0;
					newVertices[indices[numIndices]].m_uv1[1] = 0;
				}
				numIndices++;
				currentIndex++;
				numVertices++;
				numTriangles++;

				if (v[3])
				{
					originalIndex = v[IRR_TRI_1_X];
					indices[numIndices]= currentIndex;
					newVertices[currentIndex] = orgVertices[originalIndex];
					if (me->mtface)
					{
						newVertices[currentIndex].m_uv1[0] = me->mtface[t].uv[IRR_TRI_1_X][0];
						newVertices[currentIndex].m_uv1[1] = 1.f -  me->mtface[t].uv[IRR_TRI_1_X][1];
					} else
					{
						newVertices[currentIndex].m_uv1[0] = 0;
						newVertices[currentIndex].m_uv1[1] = 0;
					}
					numIndices++;
					numVertices++;
					currentIndex++;

					originalIndex =v[IRR_TRI_1_Y];
					indices[numIndices]= currentIndex;
					newVertices[currentIndex] = orgVertices[originalIndex];
					if (me->mtface)
					{
						newVertices[currentIndex].m_uv1[0] = me->mtface[t].uv[IRR_TRI_1_Y][0];
						newVertices[currentIndex].m_uv1[1] = 1.f - me->mtface[t].uv[IRR_TRI_1_Y][1];
					} else
					{
						newVertices[currentIndex].m_uv1[0] = 0;
						newVertices[currentIndex].m_uv1[1] = 0;
					}
					numIndices++;
					numVertices++;
					currentIndex++;

					originalIndex = v[IRR_TRI_1_Z];
					indices[numIndices]= currentIndex;
					newVertices[currentIndex] = orgVertices[originalIndex];
					if (me->mtface)
					{
						newVertices[currentIndex].m_uv1[0] = me->mtface[t].uv[IRR_TRI_1_Z][0];
						newVertices[currentIndex].m_uv1[1] = 1.f - me->mtface[t].uv[IRR_TRI_1_Z][1];
					} else
					{
						newVertices[currentIndex].m_uv1[0] = 0;
						newVertices[currentIndex].m_uv1[1] = 0;
					}
					
					numIndices++;
					numVertices++;
					currentIndex++;

					numTriangles++;
				}
			}

			if (numTriangles>0)
			{
				mesh->m_vertices.resize(numVertices);
				int i;

				for (i=0;i<numVertices;i++)
				{
					mesh->m_vertices[i] = newVertices[i];
				}
				mesh->m_indices.resize(numIndices);
				for (i=0;i<numIndices;i++)
				{
					mesh->m_indices[i] = indices[i];
				}
				printf("numTriangles=%d\n",numTriangles);
				//irr::scene::ISceneNode* node = createMeshNode(newVertices,numVertices,indices,numIndices,numTriangles,bulletObject,tmpObject);

				//if (!meshContainer)
				//		meshContainer = new IrrlichtMeshContainer();
						
				//meshContainer->m_userPointer = tmpObject;
				//meshContainer->m_sceneNodes.push_back(node);

				//if (newMotionState && node)
				//	newMotionState->addIrrlichtNode(node);
				

			}
		}

		/////////////////////////////
		/// Get Texture / material
		/////////////////////////////


		if (me->mtface && me->mtface[0].tpage)
		{
			//image = (Blender::Image*)m_blendFile->getMain()->findLibPointer(me->mtface[0].tpage);
			image = me->mtface[0].tpage;
		}

		if (image)
		{
			const char* fileName = image->name;
			BasicTexture* texture0 = findTexture(fileName);

			if (!texture0)
			{
				if (image->packedfile)
				{
					void* jpgData = image->packedfile->data;
					int jpgSize = image->packedfile->size;
					if (jpgSize)
					{
						texture0 = new BasicTexture((unsigned char*)jpgData,jpgSize);
						printf("texture filename=%s\n",fileName);
						texture0->loadTextureMemory(fileName);

						m_textures.insert(fileName,texture0);
					}
				}
			}

			if (!texture0)
			{
				if (!m_notFoundTexture)
				{
					int width=256;
					int height=256;
					unsigned char*  imageData=new unsigned char[256*256*3];
					for(int y=0;y<256;++y)
					{
						const int       t=y>>4;
						unsigned char*  pi=imageData+y*256*3;
						for(int x=0;x<256;++x)
						{
							const int               s=x>>4;
							const unsigned char     b=180;
							unsigned char                   c=b+((s+t&1)&1)*(255-b);
							pi[0]= 255;
							pi[1]=pi[2]=c;pi+=3;
						}
					}
					
					m_notFoundTexture = new BasicTexture(imageData,width,height);
					
				}
				texture0 = m_notFoundTexture;
			}

			if (texture0 && mesh)
			{
				float scaling[3] = {tmpObject->size.x,tmpObject->size.y,tmpObject->size.z};
				mesh->m_scaling = btVector3(scaling[IRR_X],scaling[IRR_Y],scaling[IRR_Z]);
				mesh->m_texture = texture0;
				m_renderMeshes.push_back(mesh);
			}
			return mesh;

		};
#endif //USE_GRAPHICS_OBJECTS
		return 0;
	};
};

void	SIMU::initPhysics() {
    std::cout << "\nCont3\n\n";
    const char *rutaLOG = ruta_log.c_str();
    FILE* f_log = fopen(rutaLOG,"a+");
    fprintf(f_log, "\n\nEscenario:\n");
    
    S=0.;
    sho = 1;  // 1 para mostrar ticks
    tiempo = 0.0;
    j=0;
    guardado = false;
    cant_de_MVs=0;
    angulo = 0.;
#ifdef FORCE_ZAXIS_UP
	m_cameraUp = btVector3(0,0,1);
	m_forwardAxis = 1;
#endif
	
	gContactAddedCallback = CustomMaterialCombinerCallback;

	setTexturing(false);
	setShadows(false);
    
    if (firsttime) {
        firsttime = false;
        setCameraDistance(btScalar(SCALING*4.5));
        this->m_azi = 90;
    }
	
    
	///collision configuration contains default setup for memory, collision setup
	m_collisionConfiguration = new btDefaultCollisionConfiguration();
	m_collisionConfiguration->setConvexConvexMultipointIterations();

	///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
	m_dispatcher = new	btCollisionDispatcher(m_collisionConfiguration);

	//m_broadphase = new btDbvtBroadphase();
    int  maxProxies = 1024;
	btVector3 worldAabbMin(-10000,-10000,-10000);
	btVector3 worldAabbMax( 10000, 10000, 10000);
	m_broadphase = new bt32BitAxisSweep3(worldAabbMin,worldAabbMax,maxProxies);
    
    
	///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
	btSequentialImpulseConstraintSolver* sol = new btSequentialImpulseConstraintSolver;
	m_solver = sol;
    
    //CCD or Continuous collision detection (motion clamping) causes restitution issues. So we use DCD
	m_dynamicsWorld = new btDiscreteDynamicsWorld(m_dispatcher,m_broadphase,m_solver,m_collisionConfiguration);

#ifdef FORCE_ZAXIS_UP
	m_dynamicsWorld->setGravity(btVector3(0,0,-9.81));
#else
	m_dynamicsWorld->setGravity(btVector3(0,-9.81,0));
#endif
    m_dynamicsWorld->setInternalTickCallback(TickCallback,this,true);
    
	m_dynamicsWorld->getSolverInfo().m_solverMode |= SOLVER_DISABLE_VELOCITY_DEPENDENT_FRICTION_DIRECTION+SOLVER_USE_2_FRICTION_DIRECTIONS;
	m_dynamicsWorld->getSolverInfo().m_solverMode |= SOLVER_ENABLE_FRICTION_DIRECTION_CACHING;
    
    //para mejorar la restitución
    m_dynamicsWorld->getSolverInfo().m_splitImpulse = true;
    
    
	m_blendReader = new BasicBlendReader(m_dynamicsWorld,this);

    //El nombre del archivo blender con el escenario
	const char* fileName = nom_escena;
    
	FILE* file = fopen(fileName,"rb");
	if (!file){
        printf("file : %s not found",fileName);
        fprintf(f_log, "file : %s not found",fileName);
        fclose(f_log);
		exit(0);
    }
    
	int fileLen;
	char*memoryBuffer =  0;
	{
		long currentpos = ftell(file); /* save current cursor position */
		long newpos;
		int bytesRead = 0;

		fseek(file, 0, SEEK_END); /* seek to end */
		newpos = ftell(file); /* find position of end -- this is the length */
		fseek(file, currentpos, SEEK_SET); /* restore previous cursor position */
		
		fileLen = newpos;
		
		memoryBuffer = (char*)malloc(fileLen);
		bytesRead = fread(memoryBuffer,fileLen,1,file);
		
	}

	fclose(file);

	if (memoryBuffer && fileLen)
	{
		m_blendReader->readFile(memoryBuffer,fileLen);
	}

	if (m_blendReader)
	{
		m_blendReader->convertAllObjects();
	} else
	{
		printf("file not found\n");
	}
    
    int i,m;
    m=0; //contador para la cantidad de MVVs
    
    //cantidad de objetos en la escena
    cant_de_objetos = this->m_dynamicsWorld->getNumCollisionObjects();
        
    //cantidad de objetos de la mesa vibratoria
    /*
     Se hicieron pruebas para encontrar las fuentes de ruido numérico. Una de ellas es el tamaño, forma y transformación del CG del objeto que funciona como mesa vibratoria MV. El error se minimiza cuando la MV es un cubo unitario con CG en el centro. Para acomodar todo el escenario, la MV se compone de un conjunto de cubos como el descrito.
     */
    for (i=0; i<cant_de_objetos; i++) {
        btCollisionObject* colObj = m_dynamicsWorld->getCollisionObjectArray()[i];
        Blender::Object* obj = m_blendReader->findBlenderObject(colObj);
        if (obj) {
            if (strncmp(obj->id.name, "OBedif", 6)==0) {
                cant_de_MVs+=1;
            }
        }
    }
    std::vector<int> MV_dum ((int32_t)cant_de_MVs);
    IDEdificio = MV_dum;
    
    
    //Se inicializa una matriz con los desplazamientos y rotaciones de los objetos
    std::vector<Obj> Obj_dum ((int32_t)cant_de_objetos);
    Objetos = Obj_dum;
    //El tamaño de los vectores
    for (i=0; i<cant_de_objetos; i++) {
        float cant = tiempo_extra/deltaT;
        if (prueba_plano_inclinado) {
            cant = maxang/ang;
        }
        std::vector<float> val_dum ((int32_t)(cant));
        Objetos[i].X=val_dum;
        Objetos[i].Y=val_dum;
        Objetos[i].Z=val_dum;
        Objetos[i].rVal=val_dum;
        Objetos[i].rX=val_dum;
        Objetos[i].rY=val_dum;
        Objetos[i].rZ=val_dum;
        Objetos[i].rXdeg=val_dum;
        Objetos[i].rYdeg=val_dum;
        Objetos[i].rZdeg=val_dum;
    }
    
    printf("\nSe examinarán %d objetos:",cant_de_objetos);
    printf("\nde los cuales %d integran la MV",cant_de_MVs);
    fprintf(f_log, "\nSe examinarán %d objetos",cant_de_objetos);
    fprintf(f_log, " de los cuales %d integran la MV\n",cant_de_MVs);
    
    for (i=0;i<this->m_dynamicsWorld->getNumCollisionObjects();i++){
        btCollisionObject* colObj = m_dynamicsWorld->getCollisionObjectArray()[i];
        Blender::Object* obj = m_blendReader->findBlenderObject(colObj);
        if (obj) {
            printf("\n(%d) Examinando %s ...",i,obj->id.name);
            fprintf(f_log,"\n(%d) Examinando %s ...",i,obj->id.name);
            Objetos[i].nombre = obj->id.name;
            
            btVector3 localInertia(0.,0.,0.);
            colObj->getCollisionShape()->calculateLocalInertia(obj->mass, localInertia);
                   
            
            if (custom_margen) {
                colObj->getCollisionShape()->setMargin(margen_objetos);//0.0100000951 //0.0100001
            }
            printf("\n\nMargen Objs= %4.20f ", colObj->getCollisionShape()->getMargin());

            
            if (strncmp(obj->id.name, "OBedif", 6)==0) {
                printf(" -> (El edificio sujeto a movimiento prescrito)");
                fprintf(f_log," -> (El edificio sujeto a movimiento prescrito)");
                IDEdif = i;  
                
                IDEdificio[m]=i;
                m+=1;
                
                colObj->setFriction(1.0);
                colObj->setRestitution(1.0);
                if (custom_margen) {
                    colObj->getCollisionShape()->setMargin(margen_suelo); //0.0000001
                }
                printf("\n\nMargen Suelo= %4.20f \n", colObj->getCollisionShape()->getMargin());

            }
            
            //para leer su posición, todos los objetos deben tener un motionstate
            if (btRigidBody::upcast(colObj)->getMotionState()) {
                //si tiene un motionstate
            } else {
                printf("(set MotionState)");
                fprintf(f_log," (se asigna un MotionState)");
                //no tiene, hay que crearlo
                btTransform startTransform; startTransform.setIdentity();
                startTransform = colObj->getWorldTransform();

                //btRigidBody::upcast(colObj)->getMotionState()->getWorldTransform(startTransform);
                
                obj->mass=1.; //es conveniente que los objetos tengan masas similares para reducier el error numérico de la simulación
                btVector3 localInertia(0,0,0);
                colObj->getCollisionShape()->calculateLocalInertia(obj->mass, localInertia);
                
                btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
                btRigidBody::upcast(colObj)->setMotionState(myMotionState);
                colObj->setContactProcessingThreshold(m_defaultContactProcessingThreshold);
                
                //este flag no hace flata, ya es un rigid body cuando se hace el cast desde blender
                //btRigidBody::upcast(colObj)->setCollisionFlags( btRigidBody::upcast(colObj)->getCollisionFlags() | btCollisionObject::CO_RIGID_BODY);
                
                //este habilita el uso de fricción anisotrópica. Antes habría que asignarle valores a colObj desde obj
                //btRigidBody::upcast(colObj)->setCollisionFlags( btRigidBody::upcast(colObj)->getCollisionFlags() | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);
                
                //un objeto quinemático puede ser manipulado programáticamente, no es afectado por otros objetos o la gravedad pero si afecta a otros.
                btRigidBody::upcast(colObj)->setCollisionFlags( btRigidBody::upcast(colObj)->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
                
                printf("\n\tNew Mass\t %f",obj->mass);
                fprintf(f_log,"\n\tNew Mass\t %f",obj->mass);
            }

            fprintf(f_log,"\n\tMass\t %f",obj->mass);
            
            fprintf(f_log,"\n\tDamping\t %f",obj->damping);

            if (usar_material_global) {
                printf("\n\tMaterial Global");
                //obj->mass = global_mass;
                colObj->setFriction(global_friction);
                colObj->setRestitution(global_restitution);
                fprintf(f_log,"\n\tSe usan valores de fricción y resitución globales.        !");
            } else {
                printf("\n\tMaterial Custom");
                fprintf(f_log,"\n\tSe usan valores de fricción y resititucion por elemento, leidos de 'AnisotropicFriction'");
                Blender::vec3f hack_material = obj->anisotropicFriction;
                colObj->setFriction(hack_material.x);
                colObj->setRestitution(hack_material.y);
            }
            fprintf(f_log,"\n\tFriction\t %f",colObj->getFriction());
            fprintf(f_log,"\n\tRestitution\t %f",colObj->getRestitution());
            
            // Si la distancia es mayor que este valor, no se resuleven las colisiones.
            if (usar_limite_de_contacto_personal) {
                colObj->setContactProcessingThreshold(limite_de_contacto_custom);
                fprintf(f_log, "\n\tSe uso un limite de contacto personalizado.");
            } else {
                colObj->setContactProcessingThreshold(m_defaultContactProcessingThreshold);
            }
            //también existe el opuesto,  motionthreshold, que define el límite menor desde el cual no se hace la detección de colisiones.
            //se deja en cero.
            fprintf(f_log,"\n\t\tContactProcessingThreshold\t %f",colObj->getContactProcessingThreshold());            
            fprintf(f_log,"\n\tCcdMotionThreshold\t %f",colObj->getCcdMotionThreshold());
            fprintf(f_log,"\n\tDeactivationTime\t %f",colObj->getDeactivationTime());
            fprintf(f_log,"\n\tHitFraction\t %f",colObj->getHitFraction());
            fprintf(f_log,"\n\tActivationState\t %d",colObj->getActivationState());
            fprintf(f_log,"\n\tCollisionShapeName\t %s",colObj->getCollisionShape()->getName());
            fprintf(f_log,"\n\tCollisionShapeMargin\t %f",colObj->getCollisionShape()->getMargin());

            
            //por si los objetos edificio no están en el origen.
            btTransform startTransform; startTransform.setIdentity();
            btRigidBody::upcast(colObj)->getMotionState()->getWorldTransform(startTransform);
            Objetos[i].Xinicial =startTransform.getOrigin().x();
            Objetos[i].Yinicial =startTransform.getOrigin().y();
            Objetos[i].Zinicial =startTransform.getOrigin().z();
            fprintf(f_log,"\n\tCoordenadas originales: \t(%f , %f , %f)",Objetos[i].Xinicial,Objetos[i].Yinicial,Objetos[i].Zinicial);
            printf("\n\tCoordenadas originales: \t(%f , %f , %f)",Objetos[i].Xinicial,Objetos[i].Yinicial,Objetos[i].Zinicial);
        }
        printf("\n");
    }
    
    /*
     
	for (i=0;i<this->m_dynamicsWorld->getNumCollisionObjects();i++){
        btCollisionObject* colObj = m_dynamicsWorld->getCollisionObjectArray()[i];
        Blender::Object* obj = m_blendReader->findBlenderObject(colObj);
        if (obj) {
            std::cout<<"\nobj "<<i<<" mass="<<obj->mass;
            Blender::vec3f origen;
            origen = obj->orig;
            std::cout<<" name ="<<obj->id.name;
            
            if (strncmp(obj->id.name, "OBedificio", 10)==0) {
                std::cout<<"  El edificio!";
                btTransform startTransform; startTransform.setIdentity(); startTransform = colObj->getWorldTransform();
                btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
                btScalar masa =0.f; btVector3 localInertia = btVector3(0,0,0);
                btCollisionShape* shape = colObj->getCollisionShape();
                shape->calculateLocalInertia(masa, localInertia);
                btRigidBody::btRigidBodyConstructionInfo rbInfo(masa,myMotionState,shape,localInertia);
                rbInfo.m_friction = btScalar(0.5);
                
                m_dynamicsWorld->removeCollisionObject( colObj );
                
                
                delete colObj;

     
                

                SueloBody = new btRigidBody(rbInfo);
                SueloBody->setContactProcessingThreshold(m_defaultContactProcessingThreshold);
                m_dynamicsWorld->addRigidBody(SueloBody);
                SueloBody->setCollisionFlags( SueloBody->getCollisionFlags() | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);
                SueloBody->setCollisionFlags( SueloBody->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
                SueloBody->setActivationState(DISABLE_DEACTIVATION);
                //IDEdif = i;

                
            }
        }
    }
    i=0;
*/
    
/*
#if 0
    //importar de un .h donde se traza por código
    testBody = importar_Little_box(m_defaultContactProcessingThreshold);
    m_collisionShapes.push_back(testBody->getCollisionShape());
    testBody->setCollisionFlags(testBody->getCollisionFlags() | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);
    testBody->setCollisionFlags(testBody->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
    m_dynamicsWorld->addRigidBody(testBody);
#endif
    */
    
    fclose(f_log);
    
    somethingMore();
    
	clientResetScene();
    
    cameraPos = getCameraPosition();
    m_debugMode |= btIDebugDraw::DBG_NoHelpText;
    
    printf("\nWill save a serialized .bullet file");
    
    int maxSerializeBufferSize = 1024*1024*5;
    btDefaultSerializer*	serializer = new btDefaultSerializer(maxSerializeBufferSize);
    m_dynamicsWorld->serialize(serializer);
    
    std::string dir = direc_de_trabajo;
    dir += "serializedBulletFile.bullet";
    FILE* f2 = fopen(dir.c_str(),"wb");
    printf("\nGuardando testFile.bullet");
    if (fwrite(serializer->getBufferPointer(),serializer->getCurrentBufferSize(),1,f2)) {
        printf("...  done\n");
    }
    fclose(f2);
    delete serializer;
    
    
}


void SIMU::clientMoveAndDisplay() {
    if (ver_animacion) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
	
    bool ya_termino_el_tiempo = false;
    
    if (prueba_plano_inclinado) {
        ya_termino_el_tiempo = S < maxang/ang;
        
        if (S == -1.) {
            ya_termino_el_tiempo = false;
        }
    } else {
        ya_termino_el_tiempo = tiempo < tiempo_extra;
    }
    
    
    if (ya_termino_el_tiempo){
        std::cout<<"\n\n______________________________________________________________________________________________________________________  t:  "<<tiempo<<"seg";        
        //las variables de tiempo de simulación reportada y de análisis.
        
        //float ms = getDeltaTimeMicroseconds();
        /*
        btScalar SimulationtTimeStep = btScalar(t->DT); //en microsegundos ?
        //btScalar SimulationtTimeStep = ms / 1000000.f; //en microsegundos ?
        btScalar Internaldelta = btScalar(1.f)/btScalar(1000.f);
        */
        
        btScalar SimulationtTimeStep = simulation_time_step;
        btScalar Internaldelta = simulation_internal_delta;
        int maxsubsteps = simulation_max_mum_of_substeps;
        
        //El edificio con movimiento preescrito.
        int m;
        for (m=0; m<cant_de_MVs; m++) {
            IDEdif=IDEdificio[m];
            
            btCollisionObject* EDIFICIO = m_dynamicsWorld->getCollisionObjectArray()[IDEdif];
            //Si EDIFICIO es un cuerpo rígido con un motionstate asociado, se usa el motionstate para reportar la posición de todos los objetos y para aplicar el sismo al edificio.
            if (EDIFICIO && btRigidBody::upcast(EDIFICIO) && btRigidBody::upcast(EDIFICIO)->getMotionState()){
                if (S!= -1.)
                {
                    btTransform newTrans; newTrans.setIdentity();
                    btRigidBody::upcast(EDIFICIO)->getMotionState()->getWorldTransform(newTrans);
                    
                    if (prueba_plano_inclinado) 
                    {
                        angulo = angulo + ang;
                        newTrans.setRotation(btQuaternion(btVector3(1.,0.,0.), angulo));
                        //std::cout<<"\n  ang = "<<angulo<<"\n";
                    } else {
                        //traslación.
                        //Para que no todos los componentes de la MVV se muevan desde sus posiciones iniciales, el desplazamiento se aplica a la posición inmediata anterior.
                        std::cout<<"\n\tObjeto: "<<IDEdif;
                        std::cout<<"\n\tYdelta="<<t->Ys[S];
                        std::cout<<"\n\tZini="<<Objetos[IDEdif].Zinicial;
                        
                        newTrans.setOrigin(btVector3(t->Xs[S] + Objetos[IDEdif].Xinicial, t->Ys[S]  + Objetos[IDEdif].Yinicial, t->Zs[S]  + Objetos[IDEdif].Zinicial));
                                           
                    }
                    
                    btRigidBody::upcast(EDIFICIO)->getMotionState()->setWorldTransform(newTrans);
                }
                
                //aquí tenia antes step simulation
#ifdef ver_posicion
                if (S!= -1.)
                {
                    btTransform local;
                    btRigidBody::upcast(EDIFICIO)->getMotionState()->getWorldTransform(local);
                    std::cout<<"\n\nMVV element ("<<IDEdif<<") is at: ["<<local.getOrigin().getX()<<" , "<<local.getOrigin().getY()<<" , "<<local.getOrigin().getZ()<<"]";
                } else {
                    std::cout<<"\n\nEQ signal already finished.";
                }
#endif
            }
        }
        
        //step the simulation
        if (m_dynamicsWorld)
        {
            // virtual int	stepSimulation( btScalar timeStep,int maxSubSteps=1, btScalar fixedTimeStep=btScalar(1.)/btScalar(60.));
            m_dynamicsWorld->stepSimulation(SimulationtTimeStep,maxsubsteps,Internaldelta);
            std::cout<<"\n\nEQ step done.";
        }
        
        
        
        if (S!= -1.) {
                S +=1;
            }
        if (prueba_plano_inclinado) {
            if (S>= maxang/ang) {
                std::cout<<"\n\nTermina prueba de plano inclinado";
                t->N_dat = S-1;
                S = -1.;
            }
        } else {
            if (S>= t->N_dat) {
                //ya se terminó la señal del sismo.
                std::cout<<"\n\nEarthquake input shake signal completed.\n\n";
                S = -1.;
            }
        }
        
        
        //Recordar la posición de todos los objetos para exportarlas al final
        {
            int i;
            for (i=0;i<cant_de_objetos;i++){
                if (strncmp(Objetos[i].nombre, "OBNO", 4)!=0) {
                    //este objeto no lo mostramos ni guardamos, es escenografía
                    
                    btCollisionObject* colObj = m_dynamicsWorld->getCollisionObjectArray()[i];
                    btTransform local = colObj->getWorldTransform();
                    
                    //hay que escribir esto a una variabla y al final pasarlo a un archivo
                    Objetos[i].X[j]=local.getOrigin().getX();
                    Objetos[i].Y[j]=local.getOrigin().getY();
                    Objetos[i].Z[j]=local.getOrigin().getZ();
        
                    
                    btScalar angl = local.getRotation().getAngle();
                    btVector3 vectrAxis = local.getRotation().getAxis();
                    
                    //coordenada Axis Angle
                    Objetos[i].rVal[j]=angl;
                    Objetos[i].rX[j]=vectrAxis.getX();
                    Objetos[i].rY[j]=vectrAxis.getY();
                    Objetos[i].rZ[j]=vectrAxis.getZ();
                    
                    
                    btScalar yaw,pitch,roll;
                    local.getBasis().getEulerYPR(yaw, pitch, roll);
                    
                    //radianes
                    Objetos[i].rXdeg[j]=rad_to_deg(roll);
                    Objetos[i].rYdeg[j]=rad_to_deg(pitch);
                    Objetos[i].rZdeg[j]=rad_to_deg(yaw);
                    
                    //local.getBasis().getEulerZYX(yaw, pitch, roll);
                    //std::cout<<"\n"<<roll<<"  "<<pitch<<"  "<<yaw;
                    
                    
#ifdef ver_posicion
                    const char * stract;
                    if (colObj->isActive()) {
                        stract = "(activo)";
                    } else {
                        stract = "(sleep)";
                    }
                    printf("\n CG(%d)\tp:[%f %f %f]\t α:[%f,(%f %f %f)]\t:{%f %f %f}deg \t%s",i,Objetos[i].X[j],Objetos[i].Y[j],Objetos[i].Z[j],Objetos[i].rVal[j],Objetos[i].rX[j],Objetos[i].rY[j],Objetos[i].rZ[j],Objetos[i].rXdeg[j],Objetos[i].rYdeg[j],Objetos[i].rZdeg[j],stract);
                    //printf("\t\t : { %f %f %f }", local.getRotation().getX(),local.getRotation().getY(),local.getRotation().getZ());
                    //printf("\n\t\t\t %f [ %f %f %f ]", angl, vectrAxis.getX(), vectrAxis.getY(), vectrAxis.getZ() );
#endif                
                    
                }
            }
            std::cout<<"\n";
        }
        
//aqui iba
     
        j = j+1;
        tiempo = tiempo + t->DT;
        
        
    } else {
        //se terminó el tiempo de análisis
        if (guardado == false) {
            std::cout<<"\n\nAnalisis completed. (Extra time to conclude free vibration oscilation was provided).\n\n\t\t\tSimulation finished.";
            const char *rutaLOG = ruta_log.c_str();
            FILE* f_log = fopen(rutaLOG,"a+");
            fprintf(f_log, "\n\nTermina el analisis:");
            fprintf(f_log, "	\n\nSe guardaron los siguientes archivos con el historial de desplazamiento:\n");
            
            //Suponemos que la señal misma contienen ceros al final suficientes para permitir que los objetos lleguen al reposos.
            S=-1;
            sho = 0;
            
            //guardamos los resultados de cada objeto en un arch de texto
            int i;
            std::cout<<"\n\nEscribiendo a:\n";
            for (i=0; i<cant_de_objetos; i++) {
                if (strncmp(Objetos[i].nombre, "OBNO", 4)!=0) {
                    //la escenografía no la guardamos
                    
                    std::string str = direc_de_trabajo;
                    char buff[256] = {'\0'};
                    sprintf(buff, "OBJ_(%d)_%s",i,Objetos[i].nombre);
                    str += buff;
                    str += ".txt";
                    std::cout<<str;
                    const char *ruta_texto = str.c_str();
                    fprintf(f_log, "\t%s\n",ruta_texto);
                    
                    FILE* f_text_out = fopen(ruta_texto,"w");
                    int k;
                    int max;
                    if (prueba_plano_inclinado) {
                        max = (int32_t)(maxang/ang);
                    } else {
                        max = (int32_t)(tiempo_extra/deltaT);
                    }
                    std::cout<<"\n\n Cant de datos:"<<max;
                    for (k=0; k<max; k++) {
                        //std::cout<<"\n i= "<<i<<"\t k="<<k<<"\t Y="<<Objetos[i].Y[k];
                        //printf("\n\t[ %f %f %f ]\t[ %f %f %f ]",Objetos[i].X[k],Objetos[i].Y[k],Objetos[i].Z[k],Objetos[i].rX[k],Objetos[i].rY[k],Objetos[i].rZ[k]);

                        fprintf(f_text_out, "%f\t%f\t%f\t%f\t%f\t%f\n",Objetos[i].X[k],Objetos[i].Y[k],Objetos[i].Z[k],Objetos[i].rXdeg[k],Objetos[i].rYdeg[k],Objetos[i].rZdeg[k]);
                        //fprintf(f_text_out, "%f\t%f\n",Objetos[i].Y[k],Objetos[i].rX[k]);
                    }
                    fclose(f_text_out);
                    std::cout<<" ... done\n";
                }
            }
            fclose(f_log);
            guardado = true;
            
            
            //exportamos los keyframes para una animación en blender
            expo_anim_keyframes();
            
            std::cout<<"\ngoodbye\n\n";
            {
                FILE* f_log = fopen(rutaLOG,"a+");
                fprintf(f_log, "\n\nTermino con exito");
                fclose(f_log);
            }
            
            //se cierra la ventana de animación y termina el programa
            unsigned char key = 'q';
            DemoApplication::keyboardCallback(key,0,0);
            
        }
    }
    
    if (ver_animacion) {
        //los ejes coordenados
        GL_ShapeDrawer::drawCoordSystem();        
    
        if (m_dynamicsWorld) {
            m_dynamicsWorld->debugDrawWorld();
            renderme();
        }
        
#if 1 // Esto muestra datos en la pantalla, pero arruina las texturas 
        if (verT == 1) {
            char message[128];
            sprintf(message, "Simulation Time= %f sec", tiempo);
            SIMU::displayProfileString(10, 20, message);
        }
#endif       
        glFlush();
        glutSwapBuffers();
    } else {
        if (m_dynamicsWorld) {
            m_dynamicsWorld->debugDrawWorld();
            SIMU::clientMoveAndDisplay();
        }
    }
    

    
}

float SIMU::rad_to_deg(float v_bull){
    return v_bull * 180 / 3.1415926536;    
}


void SIMU::expo_anim_keyframes(){
    std::cout<<"\n\nExporting blender keyframes for an animation";
    std::cout<<"\n\nEscribiendo a:\n";
    std::string str = direc_de_trabajo;
    str += "Keyfr_";
    time_t tim=time(NULL);
    tm *now=localtime(&tim);
    char buff[256] = {'\0'};
    sprintf(buff, "%02dhr_%02dmin",now->tm_hour,now->tm_min);
    str += buff;
    str += ".txt";
    std::cout<<str;
    const char *ruta_texto = str.c_str();

    const char *rutaLOG = ruta_log.c_str();
    FILE* f_log = fopen(rutaLOG,"a+");
    fprintf(f_log, "\nSe preparo un script en Phyton para hacer una animacion en blender:\n");
    fprintf(f_log, "\t%s",ruta_texto);
    fclose(f_log);
    
    FILE* f_text_out = fopen(ruta_texto,"w");
    
    //El timpo real de la animación pa' poner los comentarios y controlar cada cuando se guarda el archivo
    float Time = 0.;
    float Treset = 0.;
    
    //los cuadros por segundo indicados en la frontend 
    fprintf(f_text_out, "import bpy\n");
    fprintf(f_text_out, "bpy.context.scene.render.fps = %d\n",cuadrosAnim);
    fprintf(f_text_out, "bpy.context.scene.render.fps_base = 1\n");
    
    //rango de frames
    fprintf(f_text_out, "bpy.context.scene.frame_set(0)\n");
    fprintf(f_text_out, "bpy.context.scene.frame_start=0\n");
    fprintf(f_text_out, "bpy.context.scene.frame_end=%ld\n",t->N_dat);
    
    //corregir el nombre de cada objeto
    int k,i;
    //std::cout<<"\nFixed name:";
    for (i=0; i<cant_de_objetos; i++) {
        std::string str2 = Objetos[i].nombre;
        std::string str3;
        //std::cout<<"\nsize: "<<str2.size();
        str3 = str2.substr(2,str2.size());
        str = new char[str3.length()+1];
        std::strcpy(Objetos[i].nombre,str3.c_str());
        //std::cout<<"\n\t"<<Objetos[i].nombre;
    }
    
    //Set el sistema coordenado de rotación a cada obj
    for (i=0; i<cant_de_objetos; i++) {
        fprintf(f_text_out, "bpy.ops.object.select_name(name=\"%s\")\n",Objetos[i].nombre);        
        fprintf(f_text_out, "bpy.context.selected_editable_objects[0].rotation_mode='AXIS_ANGLE'\n");
    }
    
    //keyframe inicial    
    fprintf(f_text_out, "bpy.ops.object.select_name(name=\"%s\")\n",Objetos[0].nombre);
    fprintf(f_text_out, "bpy.ops.anim.keyframe_insert_menu(type='LocRot')\n");   
    
    //guardar
    fprintf(f_text_out, "bpy.ops.wm.save_mainfile(check_existing=False)\n");
    
    fprintf(f_text_out, "# _______________________________________________________________________________________________\n");
    
    //para cada instante...
    for (k=1; k<=t->N_dat; k+=1) {
        //stamp time cada 2 segundos completos
        Time += t->DT;
        Treset += t->DT;
        if (Treset >= 2.) {
            fprintf(f_text_out, "# Tiempo = %0.3f ___________________________\n",Time);
            Treset = 0.;
            
            //y lo guardamos
            fprintf(f_text_out, "bpy.ops.wm.save_mainfile(check_existing=False)\n");
        }
        
        
        fprintf(f_text_out, "bpy.ops.screen.frame_offset(delta=1)\n");
        //la transf de cada obj...
        for (i=0; i<cant_de_objetos; i++) {
            
            //seleccionar el objeto por el nombre
            fprintf(f_text_out, "bpy.ops.object.select_name(name=\"%s\")\n",Objetos[i].nombre);
            
            //transformar location
            
            /*
            //la location es una delta respecto a la anterior
            fprintf(f_text_out, "bpy.ops.transform.translate(value=(%0.6f, %0.6f, %0.6f))\n",Objetos[i].X[k]-Objetos[i].X[k-1],Objetos[i].Y[k]-Objetos[i].Y[k-1],Objetos[i].Z[k]-Objetos[i].Z[k-1]);
            */
            //ahora para poder animar sólo los segundos de interés, borramos la posición de los objetos en cada paso y la asignamos de forma absoluta. Es más trabajo pero se ahorra al momento de animar que en realidad, es la tarea que más tiempo máquina consume.
            fprintf(f_text_out, "bpy.ops.object.location_clear()\n");
            fprintf(f_text_out, "bpy.ops.transform.translate(value=(%0.6f, %0.6f, %0.6f))\n",Objetos[i].X[k],Objetos[i].Y[k],Objetos[i].Z[k]);
            
            //los ejes cambian, no vale restar ángulos sin transformar coordenadas primero. Entonces resulta más fácil borrar la rotación y luego asignarla de forma absoluta
            fprintf(f_text_out, "bpy.ops.object.rotation_clear()\n");
            //rotación en la coordenadas Axis Angle
            fprintf(f_text_out, "bpy.ops.transform.rotate(value=(%0.6f,),axis=(%0.6f, %0.6f, %0.6f))\n",Objetos[i].rVal[k],Objetos[i].rX[k],Objetos[i].rY[k],Objetos[i].rZ[k]);
            
            //insertar el keyframe de este instante
            fprintf(f_text_out, "bpy.ops.anim.keyframe_insert_menu(type='LocRot')\n");
        }
        
    }
    fprintf(f_text_out, "bpy.ops.wm.save_mainfile(check_existing=False)\n");

    fclose(f_text_out);
    std::cout<<" ... done\n";
    
}

//para no escribir un numerote cuando fallan los índices.
float SIMU::saffe(float valor){
    if ((valor > 10.) || (valor < -10.)) {
        valor = 0.;
    }
    return valor;
}



void SIMU::load_earthquakes(bool soloY) {
    setCameraDistance(btScalar(5.));
    long j;
    t = new terremoto();
    //la cantidad de datos
    t->N_dat = cant_datos(nom[0], binarios);
    tiempo_extra = deltaT * (t->N_dat+1) + tiempo_extra;
    std::cout<<"\nCant de datos: "<<t->N_dat+1;
    std::cout<<"\nDuración del sismo: "<<(t->N_dat+1)*deltaT;
    std::cout<<"\nDuración del analisis: "<<tiempo_extra;
    
    std::vector<float> ceros ((int32_t)t->N_dat);
    for (j=0; j<=(int32_t)t->N_dat; j++) {
        ceros[j]=0.;
    }
    t->Ys = ceros;
    
    t->DT = deltaT;
    t->Ys = sismo(nom[1], deltaT,t->N_dat,binarios,integrar,Escala_Y);
    
    if (soloY) {
        t->Xs = ceros;
        t->Zs = ceros;
    } else {
        t->Xs = sismo(nom[0], deltaT,t->N_dat,binarios,integrar,Escala_X);
        t->Zs = sismo(nom[2], deltaT,t->N_dat,binarios,integrar,Escala_Z);
    }
    
    //comprobación de que se lee bien el sismo:
#if 0
    {
        std::string str = nom[0];
        str += " leido.txt";
        const char *ruta_texto = str.c_str();
        
        FILE* f_text_out = fopen(ruta_texto,"w");
        for (j=0; j<=(int32_t)t->N_dat; j++) {
            fprintf(f_text_out, "%f\n",t->Ys[j]);
        }
        fclose(f_text_out);
    }
#endif
    
    const char *rutaLOG = ruta_log.c_str();
    FILE* f_log = fopen(rutaLOG,"a+");
    fprintf(f_log,"\n\nSismos:\n\n");
    fprintf(f_log,"\tCantidad de datos: %ld\n",t->N_dat+1);
    fprintf(f_log,"\tDuracion %f segundos\n",(t->N_dat+1)*deltaT);
    fprintf(f_log,"\tDuracion del analisis %f segundos\n",tiempo_extra);
    fprintf(f_log,"\tDelta t =  %f segundos\n",deltaT);
    const char *tx_binario="";
    const char *tx_integrar="";
    if (binarios) {
        tx_binario = "Archivo binario";
    } else {
        tx_binario = "Archivo de texto";
    }
    if (integrar) {
        tx_integrar = "...aceleraciones";
    } else {
        tx_integrar = "...desplazamientos";
    }
    fprintf(f_log,"\n\tSx: %s \n\t\tEscala: %f\n\t\t%s\n\t\tLa señal es un historial de %s",nom[0],Escala_X,tx_binario,tx_integrar);
    fprintf(f_log,"\n\tSy: %s \n\t\tEscala: %f\n\t\t%s\n\t\tLa señal es un historial de %s",nom[1],Escala_Y,tx_binario,tx_integrar);
    fprintf(f_log,"\n\tSz: %s \n\t\tEscala: %f\n\t\t%s\n\t\tLa señal es un historial de %s",nom[2],Escala_Z,tx_binario,tx_integrar);
    if (integrar) {
        fprintf(f_log, "\n\tSe realizo una doble integracion numerica de la señal para obtener desplazamientos");
    }
    fclose(f_log);
    
}

void	SIMU::exitPhysics() {
	delete m_blendReader;

	//cleanup in the reverse order of creation/initialization

	//remove the rigidbodies from the dynamics world and delete them
	int i;
	for (i=m_dynamicsWorld->getNumCollisionObjects()-1; i>=0 ;i--)
	{
		btCollisionObject* obj = m_dynamicsWorld->getCollisionObjectArray()[i];
		btRigidBody* body = btRigidBody::upcast(obj);
		if (body && body->getMotionState())
		{
			delete body->getMotionState();
		}
		m_dynamicsWorld->removeCollisionObject( obj );
		delete obj;
	}

	//delete collision shapes
	for (int j=0;j<m_collisionShapes.size();j++)
	{
		btCollisionShape* shape = m_collisionShapes[j];
		delete shape;
	}

	delete m_dynamicsWorld;
	
	delete m_solver;
	
	delete m_broadphase;
	
	delete m_dispatcher;

	delete m_collisionConfiguration;

	
}

void SIMU::updateCamera1() {


	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	float rele = m_ele * 0.01745329251994329547;// rads per deg
	float razi = m_azi * 0.01745329251994329547;// rads per deg


	btQuaternion rot(m_cameraUp,razi);


	btVector3 eyePos(0,0,0);
	eyePos[m_forwardAxis] = -m_cameraDistance;

	btVector3 forward(eyePos[0],eyePos[1],eyePos[2]);
	if (forward.length2() < SIMD_EPSILON)
	{
		forward.setValue(1.f,0.f,0.f);
	}
	btVector3 right = m_cameraUp.cross(forward);
	btQuaternion roll(right,-rele);

	eyePos = btMatrix3x3(rot) * btMatrix3x3(roll) * eyePos;

	m_cameraPosition[0] = eyePos.getX();
	m_cameraPosition[1] = eyePos.getY();
	m_cameraPosition[2] = eyePos.getZ();
	m_cameraPosition += m_cameraTargetPosition;

	if (m_glutScreenWidth == 0 && m_glutScreenHeight == 0)
		return;

	btScalar aspect;
	btVector3 extents;

	if (m_glutScreenWidth > m_glutScreenHeight) 
	{
		aspect = m_glutScreenWidth / (btScalar)m_glutScreenHeight;
		extents.setValue(aspect * 1.0f, 1.0f,0);
	} else 
	{
		aspect = m_glutScreenHeight / (btScalar)m_glutScreenWidth;
		extents.setValue(1.0f, aspect*1.f,0);
	}

	
	if (m_ortho)
	{
		// reset matrix
		glLoadIdentity();
		
		
		extents *= m_cameraDistance;
		btVector3 lower = m_cameraTargetPosition - extents;
		btVector3 upper = m_cameraTargetPosition + extents;
		//gluOrtho2D(lower.x, upper.x, lower.y, upper.y);
		glOrtho(lower.getX(), upper.getX(), lower.getY(), upper.getY(),-1000,1000);
		
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		//glTranslatef(100,210,0);
	} else
	{
		if (m_glutScreenWidth > m_glutScreenHeight) 
		{
			glFrustum (-aspect, aspect, -1.0, 1.0, 1.0, 10000.0);
		} else 
		{
			glFrustum (-1.0, 1.0, -aspect, aspect, 1.0, 10000.0);
		}
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		btTransform invCam = m_blendReader->m_cameraTrans.inverse();
		float m[16];
		invCam.getOpenGLMatrix(m);
		glMultMatrixf(m);

		//gluLookAt(m_cameraPosition[0], m_cameraPosition[1], m_cameraPosition[2], 
		//	m_cameraTargetPosition[0], m_cameraTargetPosition[1], m_cameraTargetPosition[2], 
		//	m_cameraUp.getX(),m_cameraUp.getY(),m_cameraUp.getZ());
	}

}

void SIMU::renderme() {

    //DemoApplication::renderme();
    
#ifdef USE_GRAPHICS_OBJECTS	
	myinit();

	updateCamera();
	
	glDisable(GL_LIGHTING);
	//glDisable(GL_LIGHT0);
	

	if (m_blendReader)
	{
		for (int i=0;i<m_blendReader->m_renderMeshes.size();i++)
		{
			m_blendReader->m_renderMeshes[i]->render();
		}
	}
#else
	DemoApplication::renderme();
#endif //USE_GRAPHICS_OBJECTS
		
}
