//
//  Sismo.cpp
//  Cont3
//
//  Created by Marcial Alberto Contreras Zazueta on 5/12/11.
//  Copyright 2011 UNAM Facultad de Ingeniería. All rights reserved.
//

#include "Sismo.h"
#include <stdio.h>
#include <iostream>
#include <sys/stat.h>
#include <string>
#include <fstream>


long cant_datos(const char * ruta, bool binario){
    std::cout << "\nreading file contents from: "<<ruta<<"\n";
    long tamano;
    if (binario) {
        //tamaño del archivo, y cantidad de datos
        struct stat results;
        if (stat(ruta, &results) == 0) {
            std::cout << "\nfile size = "<<results.st_size<<" bytes";
        }
        tamano = results.st_size/8-1;
    } else {
        char line[80];
        int i;
        i=0;
        //como los números pueden tener casi cualquier formato, lo leemos todo para saber cual es el numero de líneas y luego lo volvemos a leer para guardarlas
        FILE *lec = fopen(ruta, "r");
        while (fgets(line, 80, lec) != NULL) {
            //std::cout<<"\n"<<line;
            i=i+1;
        }
        fclose(lec);
        tamano = i-1;
    }
    return tamano;
}

std::vector<float> sismo(const char * ruta, float DT, long tamano, bool binario, bool Integra, float escala){
    std::cout << "\nDT = "<<DT<<" seconds";
    std::cout << "\nReading from: "<<ruta<<"\n";
    long j;
    std::vector<float> SS ((int32_t)tamano);
    float val;
    
    if (binario) {
        //leer binario
        std::cout<<"\nBinario";
        FILE* eq = fopen(ruta, "rb");
        if (!eq) {
            std::cout<<"\nError reading file\n\n";
        }
        
        for (j=0; j<=tamano-1; j++) {
            fread(&val, 8, 1, eq);
            SS[j] = val;
            //std::cout<<"\n"<<j<<"\t"<<SS[j];
        }
        
        fclose(eq);
        std::cout<<" ... done reading, file is closed.\n";
    } else {
        //un archivo de texto
        std::cout<<"\nTexto";
        FILE* fr = fopen(ruta, "r");
        for (j=0; j<=tamano; j++) {
            fscanf(fr, "%g",&val);
            SS[j] = val;
            //std::cout<<"\n"<<j<<"\t"<<SS[j];
        }
        fclose(fr);
        std::cout<<" ... done reading, file is closed.\n";
    }
    
#if 1
    //lo escribimos en ascii
    {
        std::string str = ruta;
        str += " leido.txt";
        const char *ruta_texto = str.c_str();
        
        FILE* f_text_out = fopen(ruta_texto,"w");
        for (j=0; j<=tamano; j++) {
            fprintf(f_text_out, "%f\n",SS[j]);
        }
        fclose(f_text_out);
    } 
#endif
    
    //aplicamos los factores de escala
    for (j=0; j<=tamano; j++) {
        SS[j] = SS[j] * escala;
    }
    
    if (Integra){
        std::cout<<"\nCorreción de línea base en la señal de aceleración";
        //la media
        float media = 0.;
        for (j=0; j<=tamano; j++) {
            media = media + SS[j];
        }
        media = media / tamano;
        
        for (j=0; j<=tamano; j++) {
            SS[j] = SS[j] - media;
        }
        
        std::cout<<"\nDoble integración";
        //velo
        //float velo[tamano-1];
        std::vector<float> velo ((int32_t)tamano-1);
        for (j=0; j<=tamano; j++) {
            velo[j]=Trapecio(0, DT*j, j+1, SS);
        }
        
        //desp
        //float desp[tamano-1];
        std::vector<float> desp ((int32_t)tamano-1);
        for (j=0; j<=tamano; j++) {
            desp[j]=Trapecio(0, DT*j, j+1, velo);
            //std::cout<<"\n"<<i<<"\t"<<acel[i];
        }
        SS = desp;
        
#if 1
        //lo escribimos en ascii
        {
            std::string str = ruta;
            str += " integrado.txt";
            const char *ruta_texto = str.c_str();
            
            FILE* f_text_out = fopen(ruta_texto,"w");
            for (j=0; j<=tamano; j++) {
                fprintf(f_text_out, "%f\n",SS[j]);
            }
            fclose(f_text_out);
        } 
#endif
        
        std::cout<<" This signal was integrated twice.\n";
    }
    return SS;
    
    
    
    /* /.......
     
     eq *answer;
     answer = new eq();
     
     std::cout << "\nset DT = "<<DT<<" seconds";
     std::cout << "\nreading file contents from: "<<ruta<<"\n";
     //int j;
     if (binario) {
     //tamaño del archivo, y cantidad de datos
     struct stat results;
     if (stat(ruta, &results) == 0) {
     std::cout << "\nfile size = "<<results.st_size<<" bytes";
     }
     tamano = results.st_size/8-1;
     } else {
     char line[80];
     int i;
     i=0;
     //como los números pueden tener casi cualquier formato, lo leemos todo para saber cual es el numero de líneas y luego lo volvemos a leer para guardarlas
     FILE *lec = fopen(ruta, "r");
     while (fgets(line, 80, lec) != NULL) {
     i=i+1;
     }
     fclose(lec);
     tamano = i-1;
     }
     
     answer->DT = DT;
     answer->N_dat = tamano;
     
     //dimension del la variable
     float acel[tamano];
     
     if (binario) {
     //leer
     FILE* eq = fopen(ruta, "rb");
     if (!eq) {
     std::cout<<"\nError reading file\n\n";
     }
     
     for (j=0; j<=tamano-1; j++) {
     fread(&acel[j], 8, 1, eq);
     SS[j] = acel[j];
     //std::cout<<"\n"<<j<<"\t"<<acel[j];
     }
     
     fclose(eq);
     std::cout<<" done reading, file is closed.\n";
     } else {
     //un archivo de texto
     FILE* fr = fopen(ruta, "r");
     int i;
     for (i=0; i<=tamano; i++) {
     fscanf(fr, "%g",&acel[i]);
     SS[i] = acel[i];
     //std::cout<<"\n"<<i<<"\t"<<acel[i];
     }
     fclose(fr);
     }
     
     
     answer->S = acel;
     
     {
     //lo escribimos en ascii
     std::string str = ruta;
     str += "leido.txt";
     const char *ruta_texto = str.c_str();
     
     FILE* f_text_out = fopen(ruta_texto,"w");
     for (j=0; j<=tamano; j++) {
     fprintf(f_text_out, "%f\n",SS[j]);
     }
     fclose(f_text_out);
     }
     
     if (Integra){
     //velo
     int i;
     float velo[tamano-1];
     for (i=0; i<=tamano; i++) {
     velo[i]=TrapeciosRule(0, DT*i, i+1, acel);
     }
     
     //desp
     float desp[tamano-1];
     for (i=0; i<=tamano; i++) {
     desp[i]=TrapeciosRule(0, DT*i, i+1, velo);
     //std::cout<<"\n"<<i<<"\t"<<acel[i];
     }
     answer->S = desp;
     }
     return SS;
     
     //return answer;
     
     */
}
float TrapeciosRule(float aa, float b, float N, float *S){
    //Apostol 735
    long i;
    float ss;
    ss= 0.;
    for (i=1; i<=(int32_t)N-1; i++) {
        ss = ss + S[i];
        //std::cout<<"\nS="<<S[i]<<"\tss="<<ss;
    }
    //std::cout<<"\n returnting = "<< 0.5 * (b - aa) * (S[1] + 2 * ss + S[(int32_t)N]) / N;
    return 0.5 * (b - aa) * (S[1] + 2 * ss + S[(int32_t)N]) / N;
}


float Trapecio(float aa, float b, float N, std::vector<float> S){
    //Apostol 735
    long i;
    float ss;
    ss= 0.;
    for (i=1; i<=(int32_t)N-1; i++) {
        ss = ss + S[i];
        //std::cout<<"\nS="<<S[i]<<"\tss="<<ss;
    }
    //std::cout<<"\n returnting = "<< 0.5 * (b - aa) * (S[1] + 2 * ss + S[(int32_t)N]) / N;
    return 0.5 * (b - aa) * (S[1] + 2 * ss + S[(int32_t)N]) / N;
}