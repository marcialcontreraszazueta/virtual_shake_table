//
//  OBj.h
//  MVV
//
//  Created by Marcial Alberto Contreras Zazueta on 10/27/11.
//  Copyright 2011 UNAM Facultad de Ingeniería. All rights reserved.
//

#include <vector>

class Obj {
    
    
public:
    char *nombre;
    
    std::vector<float> X;
    std::vector<float> Y;
    std::vector<float> Z;
    
    std::vector<float> rVal;
    std::vector<float> rX;
    std::vector<float> rY;
    std::vector<float> rZ;
    
    std::vector<float> rXdeg;
    std::vector<float> rYdeg;
    std::vector<float> rZdeg;
    
    float Xinicial;
    float Yinicial;
    float Zinicial;
    
    bool es_MV;
};