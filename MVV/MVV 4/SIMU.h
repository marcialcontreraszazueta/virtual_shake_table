/*
Bullet Continuous Collision Detection and Physics Library
Copyright (c) 2003-2006 Erwin Coumans  http://continuousphysics.com/Bullet/

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of this software.
Permission is granted to anyone to use this software for any purpose, 
including commercial applications, and to alter it and redistribute it freely, 
subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/
#ifndef SIMU_H
#define SIMU_H

#include "GlutDemoApplication.h"
#include "LinearMath/btAlignedObjectArray.h"
#include "Sismo.h"
#include <vector>
#include <string>
#include "btBulletDynamicsCommon.h"
//#include <BulletDynamics/btBulletDynamicsCommon.h>
#include "OBj.h"

class btBroadphaseInterface;
class btCollisionShape;
class btOverlappingPairCache;
class btCollisionDispatcher;
class btConstraintSolver;
struct btCollisionAlgorithmCreateFunc;
class btDefaultCollisionConfiguration;

///BasicDemo is good starting point for learning the code base and porting.
class SIMU : public GlutDemoApplication
{
    btRigidBody* SueloBody;
    btRigidBody* testBody;
    float tiempo;
    
    //btCollisionObject *Edificio;
    int IDEdif;
    std::vector<int> IDEdificio;
    
    int cant_de_objetos;
    int cant_de_MVs;
    int S;
    terremoto* t;
    
    btVector3 cameraPos;
    btVector3 *pos;
    bool firsttime;
        
    std::vector<Obj> Objetos;
    int j;
    bool guardado;
    
	//keep the collision shapes, for deletion/cleanup
	btAlignedObjectArray<btCollisionShape*>	m_collisionShapes;

	btBroadphaseInterface*	m_broadphase;

	btCollisionDispatcher*	m_dispatcher;

	btConstraintSolver*	m_solver;

	btDefaultCollisionConfiguration* m_collisionConfiguration;

	struct BasicBlendReader* m_blendReader;

	virtual void updateCamera1();

	public:
    float Escala_X;
    float Escala_Y;
    float Escala_Z;
    float global_mass;
    float global_friction;
    float global_restitution;
    const char * nom[3];
    float deltaT;
    float tiempo_extra;
    bool integrar;
    bool binarios;
    bool prueba_plano_inclinado;
    float ang;
    float angulo;
    float maxang;
    bool usar_material_global;
    bool usar_limite_de_contacto_personal;
    btScalar limite_de_contacto_custom;
    float verT;
    
    btScalar simulation_time_step;
    btScalar simulation_internal_delta;
    int simulation_max_mum_of_substeps;
    
    std::string ruta_log;
    std::string direc_de_trabajo;
    
    int cuadrosAnim;
    
    bool ver_animacion;
    
    const char *nom_escena;
    
    double margen_objetos;
    double margen_suelo;
    bool custom_margen;
    
	SIMU()
	{
		m_blendReader=0;
	}
	virtual ~SIMU()
	{
		exitPhysics();
	}
    
    void somethingMore();
    
    void    load_earthquakes(bool soloY);
    
    float    saffe(float valor);
    
    float rad_to_deg(float v_bull);
    
    void expo_anim_keyframes();
    
    virtual void keyboardCallback(unsigned char key, int x, int y);
    
	void	initPhysics();

	void	exitPhysics();

	virtual void clientMoveAndDisplay();
	virtual void displayCallback();
	
	virtual 	void renderme();

	static DemoApplication* Create()
	{
		SIMU* demo = new SIMU;
		demo->myinit();
		demo->initPhysics();
		return demo;
	}

	
};

#endif //SIMU

