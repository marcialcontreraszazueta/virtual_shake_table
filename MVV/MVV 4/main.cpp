//
//  main.cpp
//  MVV 4
//
//  Created by Marcial Contreras Zazueta on 11/19/11.
//  Copyright (c) 2011 UNAM Facultad de Ingeniería. All rights reserved.
//



#include <iostream>
#include "SIMU.h"
#include "GlutStuff.h"
#include "GLDebugDrawer.h"
#include <BulletDynamics/btBulletDynamicsCommon.h>
#include <LinearMath/btHashMap.h>
#include <stdio.h>
#include <fstream>
#include <vector>
#include <string>


class OurValue
{
    int m_uid;
    
public:
    OurValue(const btVector3& initialPos)
    :m_position(initialPos)
    {
        static int gUid=0;
        m_uid=gUid;
        gUid++;
    }
    
    btVector3	m_position;
    int	getUid() const
    {
        return m_uid;
    }
};

int main (int argc, char ** argv)
{
    GLDebugDrawer	gDebugDrawer;
    
    ///testing the btHashMap	
	btHashMap<btHashKey<OurValue>,OurValue> map;
	
	OurValue value1(btVector3(2,3,4));
	btHashKey<OurValue> key1(value1.getUid());
	map.insert(key1,value1);
    
    
	OurValue value2(btVector3(5,6,7));
	btHashKey<OurValue> key2(value2.getUid());
	map.insert(key2,value2);
	
    
	{
		OurValue value3(btVector3(7,8,9));
		btHashKey<OurValue> key3(value3.getUid());
		map.insert(key3,value3);
	}
    
    
	map.remove(key2);
    
	//const OurValue* ourPtr = map.find(key1);
	//for (int i=0;i<map.size();i++)
	//{
    //OurValue* tmp = map.getAtIndex(i);
    //printf("tmp value=%f,%f,%f\n",tmp->m_position.getX(),tmp->m_position.getY(),tmp->m_position.getZ());
	//}
    
    //leer archivo de parámetros
    if (argc != 2) {
        std::cout<<"\nError, an arguments file should be provided\n\n\n";
        exit(0);
    }
    
    char *inpath = argv[1];
    std::cout << "\nreading file contents from: "<<inpath<<"\t";
    //un archivo de texto    
    std::vector<std::string> IN;
    std::string temp;
    std::ifstream ifs(inpath);
    while (std::getline(ifs, temp)) {
        IN.push_back(temp);
        //std::cout<<"\t"<<temp<<"\n";
    }
    
    //La simulación
    SIMU ccdDemo;
    
    //Se cargan los parámetros de la simulación
    ccdDemo.nom[0] = IN[0].c_str();
    ccdDemo.nom[1] = IN[1].c_str();
    ccdDemo.nom[2] = IN[2].c_str();
    ccdDemo.deltaT = atof(IN[3].c_str());
    ccdDemo.tiempo_extra=atof(IN[4].c_str());  // en segundos
    ccdDemo.integrar = atoi(IN[5].c_str());
    ccdDemo.binarios = atoi(IN[6].c_str());
    
    ccdDemo.prueba_plano_inclinado = false;  // siempre false
    ccdDemo.ang = 0.0001;  // Delta en radianes giro alrededor de X
    ccdDemo.maxang = 0.7; // en radianes
    
    ccdDemo.nom_escena = IN[7].c_str();
    ccdDemo.Escala_X=atof(IN[8].c_str());
    ccdDemo.Escala_Y=atof(IN[9].c_str());
    ccdDemo.Escala_Z=atof(IN[10].c_str());
    ccdDemo.usar_material_global = atoi(IN[11].c_str());
    ccdDemo.global_friction = atof(IN[12].c_str());
    ccdDemo.global_restitution = atof(IN[13].c_str());
    ccdDemo.usar_limite_de_contacto_personal = atoi(IN[14].c_str());
    ccdDemo.limite_de_contacto_custom = atof(IN[15].c_str());
    ccdDemo.simulation_time_step=btScalar(atof(IN[16].c_str()));
    ccdDemo.simulation_internal_delta=btScalar(atof(IN[17].c_str()));
    ccdDemo.simulation_max_mum_of_substeps = atoi(IN[18].c_str());
    ccdDemo.ruta_log = IN[19].c_str();
    ccdDemo.ver_animacion = atoi(IN[20].c_str());
    int width = atoi(IN[21].c_str());
    int height = atoi(IN[22].c_str());
    ccdDemo.direc_de_trabajo = IN[23].c_str();
    ccdDemo.cuadrosAnim = atoi(IN[24].c_str());
    
    // 0.0100000949   el mejor para prueba a
    ccdDemo.margen_objetos = atof(IN[25].c_str()); 
    // 0.000000011 el mejor para prueba a
    ccdDemo.margen_suelo =   atof(IN[26].c_str());     
    ccdDemo.custom_margen = atoi(IN[27].c_str());
    ccdDemo.verT = atoi(IN[28].c_str());
    std::cout<<" ... done\n";

    /*
    if (ccdDemo.custom_margen) {
        printf("\nMargen Objs= %4.12f \t Margen Suel= %4.12f\n", ccdDemo.margen_objetos,ccdDemo.margen_suelo);
    }
    
    //printf("\n\nMargen Objs= %4.12f \t Margen Suel= %4.12f\n", ccdDemo.margen_objetos,ccdDemo.margen_suelo);
    */
    
    
    //ccdDemo.nom_escena = "/Users/Marshall/Master/Tesis/Tesis/Calibración/testCases/caso_g/obj_0.1_0.4_anim.blend";
    //ccdDemo.Escala_Y=0;
    
    /*
    ccdDemo.nom[0] = "/Users/Marshall/Master/Tesis/Tesis/Calibración/testCases/caso_g/senoA4W10.txt Xs.txt";
    ccdDemo.nom[1] = "/Users/Marshall/Master/Tesis/Tesis/Calibración/testCases/caso_g/senoA4W10.txt Xs.txt";
    ccdDemo.nom[2] = "/Users/Marshall/Master/Tesis/Tesis/Calibración/testCases/caso_g/senoA4W10.txt Xs.txt";
    ccdDemo.deltaT = 0.033;
    ccdDemo.tiempo_extra=0.; // * deltaT
    ccdDemo.integrar = false;
    ccdDemo.binarios = false;
    
    ccdDemo.prueba_plano_inclinado = false;
    ccdDemo.ang = 0.;
    
    ccdDemo.nom_escena = "/Users/Marshall/Master/Tesis/Tesis/Calibración/testCases/caso_g/obj_0.1_0.4.blend";
    ccdDemo.Escala_X=0.;
    ccdDemo.Escala_Y=1.;
    ccdDemo.Escala_Z=0.;
    
//    ccdDemo.global_mass = 10;
    ccdDemo.usar_material_global = false;
    ccdDemo.global_friction = 0.72;
    ccdDemo.global_restitution = 0.91;
    
    ccdDemo.usar_limite_de_contacto_personal = false;
    ccdDemo.limite_de_contacto_custom = 3.;
    
    ccdDemo.simulation_time_step=btScalar(ccdDemo.deltaT);
    ccdDemo.simulation_internal_delta=btScalar(0.001f);
    ccdDemo.simulation_max_mum_of_substeps = 300;
    
    ccdDemo.ruta_log = "/Users/Marshall/Master/Tesis/Tesis/Calibración/log_";
    */
    time_t tim=time(NULL);
    tm *now=localtime(&tim);
    char buff[256] = {'\0'};
    sprintf(buff, "%02dhr_%02dmin",now->tm_hour,now->tm_min);
    ccdDemo.ruta_log += buff;
    ccdDemo.ruta_log += ".txt";
    sprintf(buff, "%02d/%02d/%04d \t %02dhr_%02dmin",now->tm_mday,now->tm_mon,now->tm_year+1900,now->tm_hour,now->tm_min);
    
    const char *rutaLOG = ccdDemo.ruta_log.c_str();
    FILE* f_log = fopen(rutaLOG,"a+");
    fprintf(f_log, "MVV 4.0\t");
    fprintf(f_log, "%s\n\n",buff);
    fprintf(f_log, "\nSimulacion:\n\n\tTime_step\t%f\n\tInternal_delta\t%f\n\tMax_substeps\t%d\n",ccdDemo.simulation_time_step,ccdDemo.simulation_internal_delta,ccdDemo.simulation_max_mum_of_substeps);
    fclose(f_log);
    
    //                               solo Y
    ccdDemo.load_earthquakes(false);
	ccdDemo.initPhysics();
	ccdDemo.getDynamicsWorld()->setDebugDrawer(&gDebugDrawer);
    
    //ccdDemo.ver_animacion = false;
    if (ccdDemo.ver_animacion) {
        std::cout << "Generando presentación gráfica.\n";
        return glutmain(argc, argv,width,height,"MVV 4.0 Building Contents Interaction Modeling",&ccdDemo);
    } else {
        std::cout<<"No se genera presentación gráfica.\n";
        ccdDemo.moveAndDisplay();
    }
}

