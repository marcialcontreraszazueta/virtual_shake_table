//
//  KinemMot.h
//  Cont3
//
//  Created by Marcial Alberto Contreras Zazueta on 5/11/11.
//  Copyright 2011 UNAM Facultad de Ingeniería. All rights reserved.
//

//#include "btBulletDynamicsCommon.h"
#include <BulletDynamics/btBulletDynamicsCommon.h>

#include "LinearMath/btAlignedObjectArray.h"


class MyKinematicMotionState : public btMotionState {
public:
    MyKinematicMotionState(const btTransform &initialpos) {
        mPos1 = initialpos;
    }
    
    virtual ~ MyKinematicMotionState() {
    }
    
    /*void setNode(Ogre::SceneNode *node) {
     mVisibleobj = node;
     }*/
    
    virtual void getWorldTransform(btTransform &worldTrans) const {
        worldTrans = mPos1;
    }
    
    void setKinematicPos(btTransform &currentPos) {
        mPos1 = currentPos;
    }
    
    virtual void setWorldTransform(const btTransform &worldTrans) {
    }
    
protected:
    btTransform mPos1;
};