//
//  main.m
//  MVV4-launch
//
//  Created by Marcial Contreras Zazueta on 11/30/11.
//  Copyright (c) 2011 UNAM Facultad de Ingeniería. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    
    return NSApplicationMain(argc, (const char **)argv);
}
