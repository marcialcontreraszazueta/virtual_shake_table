//
//  AppDelegate.h
//  MVV4-launch
//
//  Created by Marcial Contreras Zazueta on 11/30/11.
//  Copyright (c) 2011 UNAM Facultad de Ingeniería. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate,NSTextViewDelegate>{
    IBOutlet NSWindow *window;
}

@property (assign) IBOutlet NSTextField *escala_x;
@property (assign) IBOutlet NSTextField *escala_y;
@property (assign) IBOutlet NSTextField *escala_z;
@property (assign) IBOutlet NSTextField *texto_sx;
@property (assign) IBOutlet NSTextField *texto_sy;
@property (assign) IBOutlet NSTextField *texto_sz;
@property (assign) IBOutlet NSMatrix *tipo_de_senal;
@property (assign) IBOutlet NSTextField *delta_t;
@property (assign) IBOutlet NSTextField *tiempo_extra;
@property (assign) IBOutlet NSMatrix *tipo_de_archivo;
@property (assign) IBOutlet NSTextField *texto_escenario;
@property (assign) IBOutlet NSButton *usar_globales;
@property (assign) IBOutlet NSTextField *friccion_global;
@property (assign) IBOutlet NSTextField *restitucion_global;
@property (assign) IBOutlet NSButton *usar_limite_de_contacto;
@property (assign) IBOutlet NSTextField *limite_contacto;
@property (assign) IBOutlet NSTextField *simul_deltaT;
@property (assign) IBOutlet NSTextField *simul_pasoInterno;
@property (assign) IBOutlet NSTextField *simul_maxNumStep;
@property (assign) IBOutlet NSTextField *carpeta_salida;
@property (assign) IBOutlet NSButton *usar_ver_animacion;
@property (assign) IBOutlet NSTextField *alturaVentana;
@property (assign) IBOutlet NSTextField *anchuraVentana;
@property (assign) IBOutlet NSTextView *t_out;
@property (assign) IBOutlet NSTextField *t_fps;
@property (assign) IBOutlet NSTextField *margenObjetos;
@property (assign) IBOutlet NSTextField *margenMesa;
@property (assign) IBOutlet NSButton *UsarMargenCustom;
@property (assign) IBOutlet NSButton *verTiempo;
@property (assign) IBOutlet NSButton *cerarAlterminar;
@property (assign) IBOutlet NSButton *simularFractura;
@property (assign) IBOutlet NSTextField *factor_f;
@property (assign) IBOutlet NSTextField *decimarFPS;


//@property (assign) IBOutlet NSWindow *window;




- (IBAction)busca_sx:(id)sender;
- (IBAction)busca_sy:(id)sender;
- (IBAction)busca_sz:(id)sender;
- (IBAction)busca_escenario:(id)sender;
- (IBAction)click_usar_globales:(id)sender;
- (IBAction)click_usar_limite_contacto:(id)sender;
- (IBAction)elegir_carpeta_salida:(id)sender;
- (IBAction)click_ver_animacion:(id)sender;
- (IBAction)ejecutarSimulacion:(id)sender;
- (IBAction)click_usar_margen:(id)sender;
- (IBAction)click_simular_fractura:(id)sender;

-(NSString*) pedir_un_archivo:(NSString*) mensaje o_un_directorio:(bool)dir;




@end
