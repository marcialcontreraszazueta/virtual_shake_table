//
//  AppDelegate.m
//  MVV4-launch
//
//  Created by Marcial Contreras Zazueta on 11/30/11.
//  Copyright (c) 2011 UNAM Facultad de Ingeniería. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

@synthesize escala_x = _escala_x;
@synthesize escala_y = _escala_y;
@synthesize escala_z = _escala_z;
@synthesize texto_sx = _texto_sx;
@synthesize texto_sy = _texto_sy;
@synthesize texto_sz = _texto_sz;
@synthesize tipo_de_senal = _tipo_de_senal;
@synthesize delta_t = _delta_t;
@synthesize tiempo_extra = _tiempo_extra;
@synthesize tipo_de_archivo = _tipo_de_archivo;
@synthesize texto_escenario = _texto_escenario;
@synthesize usar_globales = _usar_globales;
@synthesize friccion_global = _friccion_global;
@synthesize restitucion_global = _restitucion_global;
@synthesize usar_limite_de_contacto = _usar_limite_de_contacto;
@synthesize limite_contacto = _limite_contacto;
@synthesize simul_deltaT = _simul_deltaT;
@synthesize simul_pasoInterno = _simul_pasoInterno;
@synthesize simul_maxNumStep = _simul_maxNumStep;
@synthesize carpeta_salida = _carpeta_salida;
@synthesize usar_ver_animacion = _usar_ver_animacion;
@synthesize alturaVentana = _alturaVentana;
@synthesize anchuraVentana = _anchuraVentana;
@synthesize t_out = _t_out;
@synthesize t_fps = _t_fps;
@synthesize margenObjetos = _margenObjetos;
@synthesize margenMesa = _margenMesa;
@synthesize UsarMargenCustom = _UsarMargenCustom;
@synthesize verTiempo = _verTiempo;
@synthesize cerarAlterminar = _cerarAlterminar;
@synthesize simularFractura = _simularFractura;
@synthesize factor_f = _factor_f;
@synthesize decimarFPS = _decimarFPS;
//@synthesize window = _window;

- (void)dealloc
{
    [super dealloc];
}
	
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSString *help;
    help = [NSString stringWithFormat:@"\t\t\t\t\tMVV 5.0\n\nEn la vista previa la posición de la cámara puede cambiarse tecleando: \n\ta\ty-1 \n\td\ty+1 \n\tw\tx+1 \n\ts\ty-1 \n\tz\tZoom + \n\tx\tZoom - \n\n\tf\tMostrar ejes locales \n\tr\tMostrar aabb. \n\to\tViste en planta \n\n  espacio\t Reiniciar simulación \n\n\tq\t(1)Interrumpir simulación\n\t \t(2)Si la simulación ya ha terminado sólo se cierra la ventana de vista previa."];
    [_t_out setString:help];
    
    // load existing simu file
    NSString *t;
    t = [NSString stringWithFormat:@"%@/simu.txt",[[NSBundle mainBundle] resourcePath]];    
    NSLog(@"\n\nCargando argumentos pasados\n%@",t);
    
    // read everything from text
    NSString* fileContents = 
    [NSString stringWithContentsOfFile:t 
                              encoding:NSUTF8StringEncoding error:nil];
    
    // first, separate by new line
    NSArray* allLinedStrings = 
    [fileContents componentsSeparatedByCharactersInSet:
     [NSCharacterSet newlineCharacterSet]];
    
    [_texto_sx setStringValue:[allLinedStrings objectAtIndex:0]];
    [_texto_sy setStringValue:[allLinedStrings objectAtIndex:1]];
    [_texto_sz setStringValue:[allLinedStrings objectAtIndex:2]];
    [_delta_t setStringValue:[allLinedStrings objectAtIndex:3]];
    [_tiempo_extra setStringValue:[allLinedStrings objectAtIndex:4]];
    NSArray *celula = [_tipo_de_senal cells];
    if ([[allLinedStrings objectAtIndex:5] isEqualTo: @"0"]) {
        [[celula objectAtIndex:0] setState:1];
        [[celula objectAtIndex:1] setState:0];
    } else {
        [[celula objectAtIndex:0] setState:0];
        [[celula objectAtIndex:1] setState:1];
    }
    celula = nil;
    celula = [_tipo_de_archivo cells];
    if ([[allLinedStrings objectAtIndex:6] isEqualTo: @"0"]) {
        [[celula objectAtIndex:0] setState:1];
        [[celula objectAtIndex:1] setState:0];
    } else {
        [[celula objectAtIndex:0] setState:0];
        [[celula objectAtIndex:1] setState:1];
    }
    [_texto_escenario setStringValue:[allLinedStrings objectAtIndex:7]];
    [_escala_x setStringValue:[allLinedStrings objectAtIndex:8]];
    [_escala_y setStringValue:[allLinedStrings objectAtIndex:9]];
    [_escala_z setStringValue:[allLinedStrings objectAtIndex:10]];
    if ([[allLinedStrings objectAtIndex:11] isEqualTo: @"1"]) {
        [_usar_globales setState:1];
    } else {
        [_usar_globales setState:0];
    }
    [_friccion_global setStringValue:[allLinedStrings objectAtIndex:12]];
    [_restitucion_global setStringValue:[allLinedStrings objectAtIndex:13]];
    if ([[allLinedStrings objectAtIndex:14] isEqualTo: @"1"]) {
        [_usar_limite_de_contacto setState:1];
    } else {
        [_usar_limite_de_contacto setState:0];
    }
    [_limite_contacto setStringValue:[allLinedStrings objectAtIndex:15]];
    [_simul_deltaT setStringValue:[allLinedStrings objectAtIndex:16]];
    [_simul_pasoInterno setStringValue:[allLinedStrings objectAtIndex:17]];
    [_simul_maxNumStep setStringValue:[allLinedStrings objectAtIndex:18]];
    [_carpeta_salida setStringValue:[allLinedStrings objectAtIndex:19]];
        
    if ([[allLinedStrings objectAtIndex:20] isEqualTo: @"1"]) {
        [_usar_ver_animacion setState:1];
    } else {
        [_usar_ver_animacion setState:0];
    }
    [_alturaVentana setStringValue:[allLinedStrings objectAtIndex:21]];
    [_anchuraVentana setStringValue:[allLinedStrings objectAtIndex:22]];
    [_carpeta_salida setStringValue:[allLinedStrings objectAtIndex:23]];
    [_t_fps setStringValue:[allLinedStrings objectAtIndex:24]];
    
    [_margenObjetos setStringValue:[allLinedStrings objectAtIndex:25]];
    [_margenMesa setStringValue:[allLinedStrings objectAtIndex:26]];
    
    if ([[allLinedStrings objectAtIndex:27] isEqualTo: @"1"]) {
        //on
        [_UsarMargenCustom setState:1];
        [_margenMesa setEnabled:true];
        [_margenObjetos setEnabled:true];
    } else {
        [_UsarMargenCustom setState:0];
        [_margenMesa setEnabled:false];
        [_margenObjetos setEnabled:false];        
    }
    if ([[allLinedStrings objectAtIndex:28] isEqualTo: @"1"]) {
        //on
        [_verTiempo setState:1];
    } else {
        [_verTiempo setState:0];
    }
    if ([[allLinedStrings objectAtIndex:29] isEqualTo: @"1"]) {
        [_simularFractura setState:1];
    } else {
        [_simularFractura setState:0];
    }
    if ([[allLinedStrings objectAtIndex:30] isEqualTo: @"1"]) {
        [_cerarAlterminar setState:1];
    }  else {
        [_cerarAlterminar setState:0];
    }
    [_factor_f setStringValue:[allLinedStrings objectAtIndex:31]];
    [_decimarFPS setStringValue:[allLinedStrings objectAtIndex:32]];
    
    NSLog(@"\ndone.");

}

-(NSString*) pedir_un_archivo:(NSString*) mensaje o_un_directorio:(bool)dir {
    NSOpenPanel* panel = [NSOpenPanel openPanel];
    [panel setCanChooseDirectories:dir];
    [panel setCanChooseFiles:!dir];
    [panel setAllowsMultipleSelection:NO];
    [panel setMessage:mensaje];
    
    if (dir) {
        [panel setCanCreateDirectories:YES];
    }
    /* / Display the panel attached to the document's window.
    [panel beginSheetModalForWindow:_window completionHandler:^(NSInteger result){
        if (result == NSFileHandlingPanelOKButton) {
            NSString* arch = [panel filename];
            NSLog(@"\n%@",arch);
        }
    }]; */
    if ([panel runModalForDirectory:nil file:nil] == NSOKButton) {
        if (dir) {
            return [panel directory];
        } else {
            return [panel filename];
        }
    } else {
        return nil;
    }
}

- (IBAction)busca_sx:(id)sender {
    NSString* tx;
    tx = [[NSString alloc] initWithFormat:@"Elige un sismo para x"];
    tx = [self pedir_un_archivo: tx o_un_directorio:NO];
    if (tx) {
        [_texto_sx setStringValue:tx];
    }
}

- (IBAction)busca_sy:(id)sender {
    NSString* tx;
    tx = [[NSString alloc] initWithFormat:@"Elige un sismo para y"];
    tx = [self pedir_un_archivo: tx o_un_directorio:NO];
    if (tx) {
        [_texto_sy setStringValue:tx];
    }
}

- (IBAction)busca_sz:(id)sender {
    NSString* tx;
    tx = [[NSString alloc] initWithFormat:@"Elige un sismo para z"];
    tx = [self pedir_un_archivo: tx o_un_directorio:NO];
    if (tx) {
        [_texto_sz setStringValue:tx];
    }
}

- (IBAction)busca_escenario:(id)sender {
    NSString* tx;
    tx = [[NSString alloc] initWithFormat:@"Elige un escenario de blender"];
    tx = [self pedir_un_archivo: tx o_un_directorio:NO];
    if (tx) {
        [_texto_escenario setStringValue:tx];
    }
}

- (IBAction)click_usar_globales:(id)sender {
    if ([_usar_globales state] == 1) {
        //está ON,
        [_friccion_global setEnabled:true];
        [_restitucion_global setEnabled:true];
    } else {
        [_friccion_global setEnabled:false];
        [_restitucion_global setEnabled:false];
    }
}

- (IBAction)click_usar_limite_contacto:(id)sender {
    if ([_usar_limite_de_contacto state] == 1) {
        //está ON,
        [_limite_contacto setEnabled:true];
    } else {
        [_limite_contacto setEnabled:false];
    }
}

- (IBAction)elegir_carpeta_salida:(id)sender {
    NSString* tx;
    tx = [[NSString alloc] initWithFormat:@"¿Dónde guardar los archivos?"];
    tx = [self pedir_un_archivo: tx o_un_directorio:YES];
    if (tx) {
        tx = [NSString stringWithFormat:@"%@/",tx];
        [_carpeta_salida setStringValue:tx];
    }
}

- (IBAction)click_ver_animacion:(id)sender {
    if ([_usar_ver_animacion state] == 1) {
        //está ON,
        [_alturaVentana setEnabled:true];
        [_anchuraVentana setEnabled:true];
        [_verTiempo setEnabled:true];
        [_cerarAlterminar setEnabled:true];
    } else {
        [_alturaVentana setEnabled:false];
        [_anchuraVentana setEnabled:false];
        [_verTiempo setEnabled:false];
        [_cerarAlterminar setEnabled:false];
    }
}

- (IBAction)click_usar_margen:(id)sender {
    if ([_UsarMargenCustom state] == 1) {
        //está ON,
        [_margenObjetos setEnabled:true];
        [_margenMesa setEnabled:true];
    } else {
        [_margenObjetos setEnabled:false];
        [_margenMesa setEnabled:false];
    }
}

- (IBAction)click_simular_fractura:(id)sender {
    if ([_simularFractura state] == 1) {
        //está on
        [_factor_f setEnabled:true];
    } else {
        [_factor_f setEnabled:false];
    }
}

- (IBAction)ejecutarSimulacion:(id)sender {
    NSString *t;
    t = [[NSString alloc] initWithFormat:@"Se está realizando la simulación con los\nparámetros indicados.\n\nEspere..."];
    [_t_out setString:t];
    [_t_out needsDisplay];
    
    //checar qeu todos los campos estén completos
    if ([[_texto_sy stringValue] isEqualToString:@""]) {
        t = [NSString stringWithFormat:@"%@\n\nUPS, por mo menos debe elegir un sismo para Y",t];
        [_t_out setString:t];
        [self performSelector:NSSelectorFromString(@"busca_sy:") withObject:nil];
        return;
    } else {
        if ([[_texto_sx stringValue] isEqualToString:@""] || [[_texto_sz stringValue] isEqualToString:@""]) {
            [_texto_sx setStringValue:[_texto_sy stringValue]];
            [_texto_sz setStringValue:[_texto_sy stringValue]];
        }
    }
    
    if ([[_texto_escenario stringValue] isEqualToString:@""]) {
        t = [NSString stringWithFormat:@"%@\n\nUPS, por mo menos debe elegir un escenario\n\nLos escenarios son archivos de .blender con un objeto estático de nombre 'edificio' de dimensiones 1x1x1 (puede ser un objeto compuesto con varios cubos de 1x1x1) y uno o varios cuerpos rigidos sobre este. La fricción y restitución de los cuerpos rígidos se asume como los valores para X y Y de la fricción anisotrópica.",t];
        [self performSelector:NSSelectorFromString(@"busca_escenario:") withObject:nil];
        [_t_out setString:t];
        return;
    }
    
    if ([[_carpeta_salida stringValue] isEqualToString:@""]) {
        t = [NSString stringWithFormat:@"%@\n\nUPS, debes indicar la carpeta donde se guardarán los archivos de salida",t];
        [_t_out setString:t];
        [self performSelector:NSSelectorFromString(@"elegir_carpeta_salida:") withObject:nil];
        return;

    }
    
    //creamos el archivo de datos
    t = [NSString stringWithFormat:@"%@/simu.txt",[[NSBundle mainBundle] resourcePath]];    
    NSLog(@"\n\nEscribiendo archivo de argumentos\n%@",t);
    
    FILE* f_log = fopen([t cStringUsingEncoding:NSUTF8StringEncoding],"w");
    
    fprintf(f_log, "%s\n", [_texto_sx.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    fprintf(f_log, "%s\n", [_texto_sy.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    fprintf(f_log, "%s\n", [_texto_sz.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    fprintf(f_log, "%s\n", [_delta_t.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    fprintf(f_log, "%s\n", [_tiempo_extra.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    
    NSArray *celula = [_tipo_de_senal cells];
    if ([[celula objectAtIndex:0] state] == 1) {
        //desplazamientos
        fprintf(f_log, "0\n");
    } else {
        fprintf(f_log, "1\n");
    }
    celula = nil;
    celula = [_tipo_de_archivo cells];
    if ([[celula objectAtIndex:0] state] == 1) {
        //texto
        fprintf(f_log, "0\n");
    } else {
        fprintf(f_log, "1\n");
    }
    
    fprintf(f_log, "%s\n", [_texto_escenario.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    fprintf(f_log, "%s\n", [_escala_x.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    fprintf(f_log, "%s\n", [_escala_y.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    fprintf(f_log, "%s\n", [_escala_z.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    
    if ([_usar_globales state] == 1) {
        //está ON,
        fprintf(f_log, "1\n");
        fprintf(f_log, "%s\n", [_friccion_global.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
        fprintf(f_log, "%s\n", [_restitucion_global.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    } else {
        fprintf(f_log, "0\n0.\n0.\n");
    }
    if ([_usar_limite_de_contacto state] == 1) {
        //está ON,
        fprintf(f_log, "1\n");
        fprintf(f_log, "%s\n", [_limite_contacto.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    } else {
        //está ON,
        fprintf(f_log, "0\n");
        fprintf(f_log, "%s\n", [_limite_contacto.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    }
    
    fprintf(f_log, "%s\n", [_simul_deltaT.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    fprintf(f_log, "%s\n", [_simul_pasoInterno.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    fprintf(f_log, "%s\n", [_simul_maxNumStep.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    
    fprintf(f_log, "%s\n", [[NSString stringWithFormat:@"%@LOG_",_carpeta_salida.stringValue] cStringUsingEncoding:NSUTF8StringEncoding]);
    
    if ([_usar_ver_animacion state] == 1) {
        //está ON,
        fprintf(f_log, "1\n");
        fprintf(f_log, "%s\n", [_alturaVentana.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
        fprintf(f_log, "%s\n", [_anchuraVentana.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    } else {
        fprintf(f_log, "0\n");
        fprintf(f_log, "0\n");
        fprintf(f_log, "0\n");
    }
    fprintf(f_log, "%s\n", [_carpeta_salida.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    fprintf(f_log, "%s\n",[_t_fps.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    
    fprintf(f_log, "%s\n",[_margenObjetos.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    fprintf(f_log, "%s\n",[_margenMesa.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    
    if ([_UsarMargenCustom state] == 1) {
        fprintf(f_log, "1\n");
    } else {
        fprintf(f_log, "0\n");
    }
    
    if ([_verTiempo state] == 1) {
        fprintf(f_log, "1\n");
    } else {
        fprintf(f_log, "0\n");
    }
    
    if ([_simularFractura state] == 1) {
        fprintf(f_log, "1\n");
    } else {
        fprintf(f_log, "0\n");
    }
    
    if ([_cerarAlterminar state] == 1) {
        fprintf(f_log, "1\n");
    } else {
        fprintf(f_log, "0\n");
    }
    fprintf(f_log, "%s\n",[_factor_f.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    
    fprintf(f_log, "%s\n",[_decimarFPS.stringValue cStringUsingEncoding:NSUTF8StringEncoding]);
    
    
    fclose(f_log);    
    NSLog(@"\ndone.");
    
    //ejecutar el script en una terminal    
    NSTask *task;
    task = [[NSTask alloc] init];
    [task setLaunchPath: [[NSBundle mainBundle] pathForResource:@"MVV" ofType:nil]];
    
    NSArray *arguments;
    arguments = [NSArray arrayWithObjects: t, nil];
    [task setArguments: arguments];
    
    NSPipe *pipe;
    pipe = [NSPipe pipe];
    [task setStandardOutput: pipe];
    [task setStandardInput:[NSPipe pipe]];
    
    NSFileHandle *file;
    file = [pipe fileHandleForReading];
    
    [task launch];
    
    //NSData *data;
    //data = [file readDataToEndOfFile];
    
    NSMutableData *data = [NSMutableData dataWithCapacity:512];
    while ([task isRunning]) {
        [data appendData:[file readDataToEndOfFile]];
    }
    [data appendData:[file readDataToEndOfFile]];
    
    NSString *string;
    string = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    [_t_out setString:string];
    
    //[string release];
    [task release];
        
}


- (void)windowWillClose:(NSNotification *)aNotification {
	[NSApp terminate:self];
}


//radio buttons
//NSArray *cellArray = [myMatrix cells];
//[[cellArray objectAtIndex:0] setTitle:@"Apples"];
@end
