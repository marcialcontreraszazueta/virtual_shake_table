# README #

Contreras Zazueta Marcial Alberto
FACULTAD DE INGENIERÍA, INSTITUTO DE INGENIERÍA, UNAM
MÉXICO
2012

This repository makes use of the Bullet physics engine to recreate a shake table, so
it is possible to understand and visualize the seismic response of building contentes.


--- 

MVV contiene el código fuente de la implementación del motor físico Bullet physics.
El lenguaje de programación es C++ y se requiere la instalación de las librerías Bullet como frameworks en un systema Mac OSX 10.6

MVV4-launch es la interfaz de usuario para MVV, está codificado en Objective-C para Mac OSX 10.6
Este programa sólo crea un archivo de texto con el formato requerido por MVV para correr un análisis.

EJEMPLO contiene un archivo de Blender con un escenario de ejemplo y señales de sismo en formato de texto. DT = 0.033 segundos.
